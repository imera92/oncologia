-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 06-06-2017 a las 18:31:10
-- Versión del servidor: 10.1.16-MariaDB
-- Versión de PHP: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `oncologia`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `user` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `email` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `admin`
--

INSERT INTO `admin` (`id`, `user`, `password`, `email`) VALUES
(1, 'adotcom', '68b3c7fd0f75b433291c94492a87df11', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `eventos`
--

CREATE TABLE `eventos` (
  `id` int(11) NOT NULL,
  `tipo` varchar(50) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `descripcion` text NOT NULL,
  `contacto` varchar(50) NOT NULL,
  `inicio` date NOT NULL,
  `fin` date NOT NULL,
  `localizacion` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `eventos`
--

INSERT INTO `eventos` (`id`, `tipo`, `nombre`, `descripcion`, `contacto`, `inicio`, `fin`, `localizacion`) VALUES
(1, 'Capacitación Continua SEO', 'Cáncer de Páncreas', 'Programa para el IV Curso de Educación Médica Continuada en Oncología', '0912345678', '2017-09-27', '2017-09-27', 'Nacional'),
(2, 'Capacitación Continua SEO', 'Cáncer de Páncreas', 'Programa para el IV Curso de Educación Médica Continuada en Oncología', '0912345678', '2017-09-27', '2017-09-27', 'Nacional'),
(3, 'Capacitación Continua SEO', 'Cáncer de Páncreas', 'Programa para el IV Curso de Educación Médica Continuada en Oncología', '0912345678', '2017-09-27', '2017-09-27', 'Nacional'),
(4, 'Capacitación Continua SEO', 'Cáncer de Páncreas', 'Programa para el IV Curso de Educación Médica Continuada en Oncología', '0912345678', '2017-09-27', '2017-09-27', 'Nacional'),
(5, 'Capacitación Continua SEO', 'Cáncer de Páncreas', 'Programa para el IV Curso de Educación Médica Continuada en Oncología', '0912345678', '2017-09-27', '2017-09-27', 'Nacional'),
(6, 'Capacitación Continua SEO', 'Cáncer de Páncreas', 'Programa para el IV Curso de Educación Médica Continuada en Oncología', '0912345678', '2017-09-27', '2017-09-27', 'Nacional'),
(7, 'Capacitación Continua SEO', 'Cáncer de Páncreas', 'Programa para el IV Curso de Educación Médica Continuada en Oncología', '0912345678', '2017-09-27', '2017-09-27', 'Nacional'),
(8, 'Capacitación Continua SEO', 'Cáncer de Páncreas', 'Programa para el IV Curso de Educación Médica Continuada en Oncología', '0912345678', '2017-09-27', '2017-09-27', 'Nacional'),
(9, 'Capacitación Continua SEO', 'Cáncer de Páncreas', 'Programa para el IV Curso de Educación Médica Continuada en Oncología', '0912345678', '2017-09-27', '2017-09-27', 'Nacional'),
(10, 'Capacitación Continua SEO', 'Cáncer de Páncreas', 'Programa para el IV Curso de Educación Médica Continuada en Oncología', '0912345678', '2017-09-27', '2017-09-27', 'Nacional'),
(11, 'Capacitación Continua SEO', 'Cáncer de Páncreas', 'Programa para el IV Curso de Educación Médica Continuada en Oncología', '0912345678', '2017-09-27', '2017-09-27', 'Nacional'),
(12, 'Capacitación Continua SEO', 'Cáncer de Páncreas', 'Programa para el IV Curso de Educación Médica Continuada en Oncología', '0912345678', '2017-09-27', '2017-09-27', 'Nacional'),
(13, 'Capacitación Continua SEO', 'Cáncer de Páncreas', 'Programa para el IV Curso de Educación Médica Continuada en Oncología', '0912345678', '2017-09-27', '2017-09-27', 'Nacional'),
(14, 'Capacitación Continua SEO', 'Cáncer de Páncreas', 'Programa para el IV Curso de Educación Médica Continuada en Oncología', '0912345678', '2017-09-27', '2017-09-27', 'Nacional'),
(15, 'Capacitación Continua SEO', 'Cáncer de Páncreas', 'Programa para el IV Curso de Educación Médica Continuada en Oncología', '0912345678', '2017-09-27', '2017-09-27', 'Nacional'),
(16, 'Capacitación Continua SEO', 'Cáncer de Páncreas', 'Programa para el IV Curso de Educación Médica Continuada en Oncología', '0912345678', '2017-09-27', '2017-09-27', 'Nacional'),
(17, 'Capacitación Continua SEO', 'Cáncer de Páncreas', 'Programa para el IV Curso de Educación Médica Continuada en Oncología', '0912345678', '2017-09-27', '2017-09-27', 'Nacional'),
(18, 'Capacitación Continua SEO', 'Cáncer de Próstata', 'Programa para el IV Curso de Educación Médica Continuada en Oncología', '0912345678', '2017-06-07', '2017-06-07', 'Nacional'),
(19, 'Capacitación Continua SEO', 'Cuidados Paliativos	', 'Programa para el IV Curso de Educación Médica Continuada en Oncología	', '0912345678', '2017-06-08', '2017-06-08', 'Internacional'),
(20, 'Capacitación Continua SEO', 'Sarcomas', 'Programa para el IV Curso de Educación Médica Continuada en Oncología	', '0912345678', '2017-06-16', '2017-06-16', 'Nacional'),
(21, 'Capacitación Continua SEO', 'Cáncer Gástrico	', 'Programa para el IV Curso de Educación Médica Continuada en Oncología	', '0912345678', '2017-06-17', '2017-06-17', 'Internacional');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `noticias`
--

CREATE TABLE `noticias` (
  `id` int(11) NOT NULL,
  `titulo` varchar(50) NOT NULL,
  `texto` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `noticias`
--

INSERT INTO `noticias` (`id`, `titulo`, `texto`) VALUES
(1, 'Hábitos saludables en sobrevivientes al Cáncer', '<p style="text-align: justify;">\r\n	<span style="color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam volutpat ante nec imperdiet porttitor. Integer pulvinar enim purus, vitae molestie nunc pellentesque non. Mauris luctus vestibulum mauris pretium egestas. Sed euismod id lorem ac suscipit. Aenean in mi vehicula libero semper vehicula sed et magna. Ut vel ipsum non libero lacinia commodo. Suspendisse nec metus id lorem semper cursus ut eu sem. Sed condimentum ac neque a aliquam. Vestibulum euismod ligula purus, at mattis nunc facilisis ac. Morbi non velit eget leo tempus tincidunt. Pellentesque lorem nibh, accumsan sed tempor at, mattis nec nulla. Etiam eleifend commodo augue, vitae tincidunt libero sodales vel. Donec faucibus ligula eget orci semper, id rhoncus eros cursus. Sed malesuada blandit nisl, eu ullamcorper orci scelerisque accumsan. Maecenas porttitor ante in augue malesuada lacinia. Nulla fringilla luctus enim eget tempus.</span></p>\r\n');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `opiniones`
--

CREATE TABLE `opiniones` (
  `id` int(11) NOT NULL,
  `titulo` varchar(50) NOT NULL,
  `foto` varchar(50) NOT NULL,
  `descripcion` text NOT NULL,
  `doctor` varchar(50) NOT NULL,
  `fecha` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `socios`
--

CREATE TABLE `socios` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `apellido` varchar(100) NOT NULL,
  `posicion` varchar(100) NOT NULL,
  `correo` varchar(100) NOT NULL,
  `foto` varchar(225) NOT NULL,
  `ciudad` varchar(100) NOT NULL,
  `nucleo` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `socios`
--

INSERT INTO `socios` (`id`, `nombre`, `apellido`, `posicion`, `correo`, `foto`, `ciudad`, `nucleo`) VALUES
(1, 'Rosario', 'Bohorquez Jácome', 'Secretario SEO Núcleo Manabí', 'geneala157@hotmail.com', '7a04a-600x600.png', 'Manabí', 'Manabi'),
(2, 'Rosario', 'Bohorquez Jácome', 'Secretario SEO Núcleo Manabí', 'geneala157@hotmail.com', '7a04a-600x600.png', 'Manabí', 'Manabí'),
(3, 'Rosario', 'Bohorquez Jácome', 'Secretario SEO Núcleo Manabí', 'geneala157@hotmail.com', '7a04a-600x600.png', 'Manabí', 'Manabí'),
(4, 'Rosario', 'Bohorquez Jácome', 'Secretario SEO Núcleo Manabí', 'geneala157@hotmail.com', '7a04a-600x600.png', 'Manabí', 'Manabí'),
(5, 'Rosario', 'Bohorquez Jácome', 'Secretario SEO Núcleo Manabí', 'geneala157@hotmail.com', '7a04a-600x600.png', 'Manabí', 'Manabí'),
(6, 'Rosario', 'Bohorquez Jácome', 'Secretario SEO Núcleo Manabí', 'geneala157@hotmail.com', '7a04a-600x600.png', 'Manabí', 'Manabí'),
(7, 'Rosario', 'Bohorquez Jácome', 'Secretario SEO Núcleo Manabí', 'geneala157@hotmail.com', '7a04a-600x600.png', 'Manabí', 'Manabí'),
(8, 'Rosario', 'Bohorquez Jácome', 'Secretario SEO Núcleo Manabí', 'geneala157@hotmail.com', '7a04a-600x600.png', 'Manabí', 'Manabí'),
(9, 'Rosario', 'Bohorquez Jácome', 'Secretario SEO Núcleo Manabí', 'geneala157@hotmail.com', '7a04a-600x600.png', 'Manabí', 'Manabí'),
(10, 'Rosario', 'Bohorquez Jácome', 'Secretario SEO Núcleo Manabí', 'geneala157@hotmail.com', '7a04a-600x600.png', 'Manabí', 'Manabí'),
(11, 'Rosario', 'Bohorquez Jácome', 'Secretario SEO Núcleo Manabí', 'geneala157@hotmail.com', '7a04a-600x600.png', 'Manabí', 'Manabí');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user` (`user`);

--
-- Indices de la tabla `eventos`
--
ALTER TABLE `eventos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `noticias`
--
ALTER TABLE `noticias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `opiniones`
--
ALTER TABLE `opiniones`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `socios`
--
ALTER TABLE `socios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `eventos`
--
ALTER TABLE `eventos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT de la tabla `noticias`
--
ALTER TABLE `noticias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `opiniones`
--
ALTER TABLE `opiniones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `socios`
--
ALTER TABLE `socios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
