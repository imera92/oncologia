-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 25-09-2017 a las 08:06:54
-- Versión del servidor: 10.1.16-MariaDB
-- Versión de PHP: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `oncologia`
--

--
-- Volcado de datos para la tabla `admin`
--

INSERT INTO `admin` (`id`, `user`, `password`, `email`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'imera92@gmail.com');

--
-- Volcado de datos para la tabla `eventos`
--

INSERT INTO `eventos` (`id`, `tipo`, `nombre`, `descripcion`, `contacto`, `inicio`, `fin`, `localizacion`) VALUES
(1, 'Capacitación Continua SEO', 'Cáncer de Páncreas', 'Programa para el IV Curso de Educación Médica Continuada en Oncología', 'info@seo.com.ec', '2017-09-27', '2017-09-27', 'Nacional'),
(2, 'Capacitación Continua SEO', 'Cáncer de Páncreas', 'Programa para el IV Curso de Educación Médica Continuada en Oncología', 'info@seo.com.ec', '2017-08-30', '2017-08-30', 'Nacional'),
(3, 'Capacitación Continua SEO', 'Cuidados Paliativos', 'Programa para el IV Curso de Educación Médica Continuada en Oncología', 'info@seo.com.ec', '2017-07-26', '2017-07-26', 'Nacional'),
(4, 'Capacitación Continua SEO', 'Cáncer de Tiroides', 'Programa para el IV Curso de Educación Médica Continuada en Oncología', 'info@seo.com.ec', '2017-06-28', '2017-06-28', 'Nacional'),
(5, 'Capacitación Continua SEO', 'Sarcomas', 'Programa para el IV Curso de Educación Médica Continuada en Oncología', 'info@seo.com.ec', '2017-05-31', '2017-05-31', 'Nacional'),
(6, 'Capacitación Continua SEO', 'Indicaciones de PET/Carcinomas Neuroendócrinos', 'Programa para el IV Curso de Educación Médica Continuada en Oncología', 'info@seo.com.ec', '2017-04-26', '2017-04-26', 'Nacional'),
(7, 'Capacitación Continua SEO', 'Cáncer Gástrico', 'Programa para el IV Curso de Educación Médica Continuada en Oncología', 'info@seo.com.ec', '2017-03-29', '2017-03-29', 'Nacional'),
(8, 'Capacitación Continua SEO', 'Línfomas', 'Programa para el IV Curso de Educación Médica Continuada en Oncología', 'info@seo.com.ec', '2017-02-27', '2017-02-27', 'Nacional'),
(9, 'Capacitación Continua SEO', 'Cáncer de Pulmón', 'Programa para el IV Curso de Educación Médica Continuada en Oncología', 'info@seo.com.ec', '2017-01-25', '2017-01-25', 'Nacional'),
(10, 'Capacitación Continua SEO', 'Inmunoterapia Básica', 'Programa para el IV Curso de Educación Médica Continuada en Oncología', 'info@seo.com.ec', '2016-11-30', '2016-11-30', 'Nacional'),
(11, 'Capacitación Continua SEO', 'Recursos Locales para Diagnóstico por Biología Molecular', 'Programa para el IV Curso de Educación Médica Continuada en Oncología', 'info@seo.com.ec', '2016-10-26', '2016-10-26', 'Nacional'),
(12, 'Capacitación Continua SEO', 'Cáncer de Riñón', 'Programa para el III Curso de Educación Médica Continuada en Oncología', 'info@seo.com.ec', '2016-09-28', '2016-09-28', 'Nacional'),
(13, 'Capacitación Continua SEO', 'Cáncer de Ovario', 'Programa para el III Curso de Educación Médica Continuada en Oncología', 'info@seo.com.ec', '2016-08-31', '2016-08-31', 'Nacional'),
(14, 'Capacitación Continua SEO', 'Linfoma de la Zona Gris', 'Programa para el III Curso de Educación Médica Continuada en Oncología', 'info@seo.com.ec', '2016-07-27', '2016-07-27', 'Nacional'),
(15, 'Capacitación Continua SEO', 'Cáncer de Mama Temprano', 'Programa para el III Curso de Educación Médica Continuada en Oncología', 'info@seo.com.ec', '2016-06-29', '2016-06-29', 'Nacional'),
(16, 'Capacitación Continua SEO', 'Cáncer de Tiroides', 'Programa para el III Curso de Educación Médica Continuada en Oncología', 'info@seo.com.ec', '2016-05-25', '2016-05-25', 'Nacional'),
(17, 'Capacitación Continua SEO', 'Introducción a la Inmuno-Oncología', 'Programa para el III Curso de Educación Médica Continuada en Oncología', 'info@seo.com.ec', '2016-04-27', '2016-04-27', 'Nacional'),
(18, 'Capacitación Continua SEO', 'Cáncer de Pulmón', 'Programa para el III Curso de Educación Médica Continuada en Oncología', 'info@seo.com.ec', '2016-03-30', '2016-03-30', 'Nacional'),
(19, 'Capacitación Continua SEO', 'Herramientas de Inteligencia Emocional desde una Perspectiva Ontológica', 'Programa para el III Curso de Educación Médica Continuada en Oncología', 'info@seo.com.ec', '2016-02-25', '2016-02-25', 'Nacional'),
(20, 'Capacitación Continua SEO', 'Novedades en Neurocirugía Ontológica', 'Programa para el III Curso de Educación Médica Continuada en Oncología', 'info@seo.com.ec', '2016-01-27', '2016-01-27', 'Nacional'),
(21, 'Capacitación Continua SEO', 'Cáncer de Mama Avanzado', 'Programa para el III Curso de Educación Médica Continuada en Oncología', 'info@seo.com.ec', '2015-11-25', '2015-11-25', 'Nacional'),
(22, 'Capacitación Continua SEO', 'Biología Molecular', 'Programa para el III Curso de Educación Médica Continuada en Oncología', 'info@seo.com.ec', '2015-10-28', '2015-10-28', 'Nacional'),
(23, 'Capacitación Continua SEO', 'Cáncer de Ovario', 'Programa para el II Curso de Educación Médica Continuada en Oncología', 'info@seo.com.ec', '2015-09-30', '2015-09-30', 'Nacional'),
(24, 'Capacitación Continua SEO', 'Cáncer de Cérvix', 'Programa para el II Curso de Educación Médica Continuada en Oncología', 'info@seo.com.ec', '2015-08-26', '2015-08-26', 'Nacional'),
(25, 'Capacitación Continua SEO', 'Sarcoma de Partes Blandas', 'Programa para el II Curso de Educación Médica Continuada en Oncología', 'info@seo.com.ec', '2015-07-29', '2015-07-29', 'Nacional'),
(26, 'Capacitación Continua SEO', 'Melonoma', 'Programa para el II Curso de Educación Médica Continuada en Oncología', 'info@seo.com.ec', '2015-06-24', '2015-06-24', 'Nacional'),
(27, 'Capacitación Continua SEO', 'Cáncer de Riñón', 'Curso de Educación Médica Continuada en Oncología', 'info@seo.com.ec', '2015-05-27', '2015-05-27', 'Nacional'),
(28, 'Capacitación Continua SEO', 'Cáncer de Próstata', 'Curso de Educación Médica Continuada en Oncología', 'info@seo.com.ec', '2015-04-29', '2015-04-29', 'Nacional'),
(29, 'Capacitación Continua SEO', 'Diagnóstico en Biología Molecular', 'Curso de Educación Médica Continuada en Oncología', 'info@seo.com.ec', '2015-03-25', '2015-03-25', 'Nacional'),
(30, 'Capacitación Continua SEO', 'Neoplasias del sistema Nervioso Central', 'Curso de Educación Médica Continuada en Oncología', 'info@seo.com.ec', '2015-02-25', '2015-02-25', 'Nacional'),
(31, 'Capacitación Continua SEO', 'Genetica básica aplicada en Oncología', 'Curso de Educación Médica Continuada en Oncología', 'info@seo.com.ec', '2015-01-28', '2015-01-28', 'Nacional'),
(32, 'Capacitación Continua SEO', 'Cáncer de colon metastásico', 'Curso de Educación Médica Continuada en Oncología', 'info@seo.com.ec', '2014-11-26', '2014-11-26', 'Nacional'),
(33, 'Capacitación Continua SEO', 'Inmunoterapia Básica', 'Programa para el II curso de Educación Médica Continuada en Oncología', 'info@seo.com.ec', '2014-10-29', '2014-10-29', 'Nacional');

--
-- Volcado de datos para la tabla `imagenescarousel`
--

INSERT INTO `imagenescarousel` (`id`, `imagen`, `link`, `alt`, `estado`) VALUES
(1, '824e5-img.jpg', NULL, NULL, 1),
(2, '9d7c4-img.jpg', 'https://www.grocerycrud.com/documentation/options_functions', NULL, 1),
(3, '85978-img.jpg', 'http://fontawesome.io/icon/picture-o/', 'Iconos de FontAwesome', 2);

--
-- Volcado de datos para la tabla `links_mbe`
--

INSERT INTO `links_mbe` (`id`, `titulo`, `link`, `archivo`) VALUES
(1, 'Normas de Buenas Prácticas Clínicas (BPC)', NULL, '1fd36-buenas_practicas_clinicas.pdf'),
(2, 'Comités de Ética en Investigación', NULL, 'e224a-comites_etica_investigacion.pdf'),
(3, 'Declaración Universal sobre Bioética y Derechos Humanos', NULL, '3aba3-declaracion_universal_bioetcia_ddhh.pdf'),
(4, 'Revisión de Ensayos Clínicos: Una Guía para el Comité de Ética', NULL, '08fa4-revision_ensayos_clinicos.pdf'),
(5, 'Declaración de Helsinki de la Asociación Médica Mundial', NULL, '26d27-declaracion_helsinki.pdf'),
(6, 'Guías Éticas de Investigación en Biomedicina', NULL, 'b596d-guias_eticas_investigacion_biomedicina.pdf'),
(7, 'El Informe de Belmont', NULL, '4c0e4-informe_belmont.pdf');

--
-- Volcado de datos para la tabla `links_oncologia`
--

INSERT INTO `links_oncologia` (`id`, `titulo`, `link`, `archivo`) VALUES
(1, 'Memo inOncology WCLC 2016 ', NULL, 'd509a-memo_inoncology_wclc_2016.pdf'),
(2, 'Memo inOncology Special WCLC 2017', NULL, '5ff13-memo_inoncology_special_wclc_2017.pdf');

--
-- Volcado de datos para la tabla `noticias`
--

INSERT INTO `noticias` (`id`, `titulo`, `texto`, `fecha`, `imagen`) VALUES
(13, 'Test', '<p>\n	Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum.</p>\n<p>\n	Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum.</p>\n', '2017-06-26 06:29:41', '78a1b-test.png'),
(14, 'Test 2', '<p>\r\n	Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum.</p>\r\n<p>\r\n	Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum.</p>\r\n', '2017-06-26 06:30:18', ''),
(15, 'Test 3', '<p>\n	Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum <em>Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum.</em></p>\n<p>\n	Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum <strong>Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum.</strong></p>\n<ul>\n	<li>\n		Lorem Ipsum</li>\n	<li>\n		Lorem Ipsum</li>\n</ul>\n', '2017-06-26 06:41:52', ''),
(16, 'Test 4', '<p>\n	<strong>Lorem ipsum Lorem ipsum Lorem ipsum</strong> <em>Lorem ipsum Lorem ipsum</em> Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum</p>\n', '2017-06-26 07:30:45', ''),
(17, 'Test 5', '<p>\r\n	Lorem Ipsum</p>\r\n<p>\r\n	Lorem Ipsum</p>\r\n', '2017-06-26 07:51:03', ''),
(18, 'Test 6', '<p>\r\n	Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum</p>\r\n<ol>\r\n	<li>\r\n		Lorem Ipsum</li>\r\n	<li>\r\n		Lorem Ipsum</li>\r\n	<li>\r\n		Lorem Ipsum</li>\r\n	<li>\r\n		Lorem Ipsum</li>\r\n	<li>\r\n		Lorem Ipsum</li>\r\n	<li>\r\n		Lorem Ipsum</li>\r\n</ol>\r\n', '2017-06-26 08:53:43', '');

--
-- Volcado de datos para la tabla `opiniones`
--

INSERT INTO `opiniones` (`id`, `titulo`, `foto`, `descripcion`, `doctor`, `fecha`, `especializacion`) VALUES
(1, 'BIOSIMILARES', '441d3-pic.jpg', '<p>\r\n	<span style="color: rgb(51, 51, 51); font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 14px; text-align: justify;">En esta &uacute;ltima d&eacute;cada el uso de la biotecnolog&iacute;a ha permitido el desarrollo y mejoramiento de la terapia, esto incluye el &eacute;xito de la terapia con biol&oacute;gicos, Los mismos que son prote&iacute;nas desarrolladas usando la tecnolog&iacute;a del DNA recombinante.</span></p>\r\n<p style="box-sizing: border-box; margin: 0px 0px 10px; color: rgb(51, 51, 51); font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 14px; text-align: justify;">\r\n	Esta r&aacute;pida evoluci&oacute;n de la terapia biol&oacute;gica, con sus beneficios y tiempo de expiraci&oacute;n de las patentes, ha permitido la introducci&oacute;n de los Biosimilares que ha tenido su mayor desarrollo en Europa, siendo la EMA la agencia en aprobar el primer biosimilar en el 2005. Los biosimilares son productos bioterap&eacute;utico &ldquo;similares&rdquo; en t&eacute;rminos de calidad, seguridad y eficacia de un producto bioterap&eacute;utico de referencia ya utilizado, pero no son &quot;id&eacute;nticos&rdquo; OMS).</p>\r\n<div>\r\n	<p style="box-sizing: border-box; margin: 0px 0px 10px; color: rgb(51, 51, 51); font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 14px; text-align: justify;">\r\n		Aqu&iacute; es importante se&ntilde;alar que. existe un tercer grupo de medicamentos que son &ldquo;biol&oacute;gicos no comparables&quot; que no Cumplen Las exigencias para ser llamados biosimilares y son productos que cuya presencia ser&aacute; m&aacute;s frecuente en el campo m&eacute;dico si no establecen pol&iacute;ticas claras del uso de biosimilares.</p>\r\n	<div>\r\n		<p style="box-sizing: border-box; margin: 0px 0px 10px; color: rgb(51, 51, 51); font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 14px; text-align: justify;">\r\n			La equivalencia terap&eacute;utica, estudios de inmunogenicidad y La equivalencia farmacocin&eacute;tica son los determinantes en eficacia cl&iacute;nica de un biosimilar. En la actualidad los estudios comparativos de biol&oacute;gicos y biosimilares est&aacute;n fundamentados en dise&ntilde;os de no inferioridad, pero lo ideal ser&iacute;a determinar equivalencia y deber&iacute;a ser a doble ciego. La extrapolaci&oacute;n implica la utilizaci&oacute;n de biosimilares en indicaciones &ldquo;no estudiadas&quot; pero que el Producto de referencia ha sido aprobado, este constituye un problema en la decisi&oacute;n final sin embargo existen recomendaciones espec&iacute;ficas (OMS, EMA y FDA). La biosimilaridad &ldquo;no garantiza&rdquo; la intercambiabilidad que es la pr&aacute;ctica de hacer switch innovador-biosimilar, y esto se puede ver afectado m&aacute;s a&uacute;n si existe la &ldquo;sustituci&oacute;n autom&aacute;tica&rdquo; donde es el farmacista Quien reemplaza al innovador sin conocimiento del m&eacute;dico.</p>\r\n		<div>\r\n			<span style="color: rgb(51, 51, 51); font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 14px; text-align: justify;">Por lo manifestado es determinante la creaci&oacute;n de las &aacute;reas de FARMACOVIGILANCIA en las unidades hospitalarias y si las hay, habr&aacute; que fortalecerles porque as&iacute; podremos hacer un correcto monitoreo, seguimiento de los nuevos medicamentos y sus efectos adversos, ya que nosotros como equipo de salud debernos ofrecer una &oacute;ptima atenci&oacute;n con medicamentos que garanticen nuestro tratamiento, siempre pensando en proteger la salud del paciente.</span></div>\r\n	</div>\r\n</div>\r\n<p>\r\n	&nbsp;</p>\r\n', 'DR. RENÉ MUÑOZ GERMEN', '2016-11-28', 'ONCÓLOGO CLÍNICO'),
(2, 'LOREM', '2a20b-pokimon.png', '<p>\r\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas hendrerit est eget lorem consequat, in consequat eros interdum. Duis venenatis sed neque at tincidunt. Phasellus volutpat, lacus sed egestas malesuada, magna ante tincidunt orci, vitae commodo orci lectus sed diam. Morbi ornare consectetur massa eu maximus. Vivamus lobortis rutrum ligula, in lacinia diam iaculis ac. Ut porta at tellus ac pulvinar. Duis faucibus varius sagittis. Vestibulum lacinia consectetur neque, ut dapibus ipsum. Mauris at elit et leo ornare tincidunt. Nunc nec leo et mi tristique interdum sit amet non massa. Fusce semper felis eget elit tempus, at vulputate nulla bibendum. Cras malesuada odio magna, at bibendum massa tristique non.</p>\r\n', 'Ivan Mera', '2017-06-26', 'Ingniero');

--
-- Volcado de datos para la tabla `socios`
--

INSERT INTO `socios` (`id`, `nombre`, `apellido`, `posicion`, `correo`, `foto`, `ciudad`, `nucleo`, `telefono`, `celular`, `localizacion`, `destacado`) VALUES
(1, 'Rosario', 'Bohorquez Jácome', 'Secretario SEO Núcleo Manabí', 'geneala157@hotmail.com', '09557-7a04a-600x600.png', 'Manabí', 'Manabi', '042824019', '0981617261', 'Nacional', 1),
(25, 'Ivan', 'Mera', 'Ingeniero', 'imera92@gmail.com', '', 'Guayaquil', 'Guayas', '0428124019', '0981617261', 'Nacional', 1),
(26, 'Alejandro', 'Maldonado', 'Ingeniero', 'imera92@gmail.com', '', 'Manabí', 'Manabi', '', '0981617261', 'Nacional', 2),
(27, 'Juan', 'Mora', 'Licenciado', 'imera92@gmail.com', '', 'Guayaquil', 'Guayas', '', '0981617261', 'Nacional', 2),
(28, 'Ivan', 'Mora', 'Agricultor', 'imera92@gmail.com', '', 'Guayaquil', 'Guayas', '0981617261', '0981617261', 'Nacional', 2),
(29, 'Gene', 'Alarcón', 'INVITADO', 'geneala157@hotmail.com', '', 'MANABI', '', '', '', 'Nacional', 2),
(30, 'Daniel', 'Alarcón', 'PRESIDENTE SEO MANABI', 'daniel.alarcon7@hotmail.com', '', 'MANABI', 'MANABI', '', '', 'Nacional', 2),
(31, 'Ruth', 'Armijos Luna', '', 'rarmijos1_@hotmail.com', '', 'MANABI', '', '', '', 'Nacional', 2),
(32, 'Lila', 'Ayala Esparza', 'INVITADO', 'layala@solca.med.ec', '', 'GUAYAQUIL', '', '', '', 'Nacional', 2),
(33, 'Jackeline', 'Baquerizo Potes', 'INVITADO', 'Jebp64@hotmail.com', '', 'GUAYAQUIL', '', '', '', 'Nacional', 2),
(34, 'Claire', 'Barbery Gomez', 'INVITADO', 'cibarbery@yahoo.com', '', 'GUAYAQUIL', '', '', '', 'Nacional', 2),
(35, 'Rosario', '', 'INVITADO', 'rosariobjmd@hotmail.com', '', 'GUAYAQUIL', '', '', '', 'Nacional', 2),
(36, ' Camacho Alvarez Fernando', '', 'INVITADO', 'jfca@gye.satnet.net', '', 'GUAYAQUIL', '', '', '', 'Nacional', 2),
(37, 'Miguel', 'Cedeño Vera', 'SECRETARIO SEO NUCLEO MANABI', 'miguelharold@yahoo.com', '', 'MANABI', 'MANABI', '', '', 'Nacional', 2),
(38, 'Christian', 'Pais', 'VICEPRESIDENTE SEO NUCLEO QUITO', 'paischristian@hotmail.com', '', 'QUITO', 'QUITO', '', '', 'Nacional', 2),
(39, 'Lenny', 'Abad Mosquera', 'SECRETARIA SEO NUCLEO QUITO', 'leabad@intramed.net', '', 'QUITO', 'QUITO', '', '', 'Nacional', 2),
(40, 'Sandra', 'Tafur', 'TESORERA SEO NUCLEO QUITO', 's_tafur@hotmail.com', '', 'QUITO', 'QUITO', '', '', 'Nacional', 2),
(41, 'Leonardo', 'Proaño', 'VOCAL PRINCIPAL SEO NUCLEO QUITO', '', '', 'QUITO', 'QUITO', '', '', 'Nacional', 2),
(42, 'Luis', 'Pachecho Ojeda', 'VOCAL PRINCIPAL SEO NUCLEO QUITO', 'luispacheco.o@hotmail.com', '', 'QUITO', 'QUITO', '', '', 'Nacional', 2),
(43, 'Marco', 'Romero Pinos', 'VOCAL PRINCIPAL SEO NUCLEO QUITO', 'romerom1949@hotmail.com', '', 'QUITO', 'QUITO', '', '', 'Nacional', 2),
(44, 'Miguel', 'Jerves Andrade', 'SECRETARIO SEO NUCLEO CUENCA', 'mjerves@uazuay.edu.ec', '', 'CUENCA', 'CUENCA', '', '', 'Nacional', 2),
(45, 'Humberto', 'Quito Ruilova', 'VOCAL SEO NUCLEO CUENCA', 'hquito@uazuay.edu.ec', '', 'CUENCA', 'CUENCA', '', '', 'Nacional', 2),
(46, 'Ciro Patricio', 'Miranda Delgado', 'VICEPRESIDENTE SEO NUCLEO MANABI', 'gina_1025@hotmail.com', '', 'MANABI', 'MANABI', '', '', 'Nacional', 2),
(47, 'Lorena', 'Mejía Murillo', 'TESORERA SEO NUCLEO MANABI', 'lorena63.mejia@gmail.com', '', 'MANABI', 'MANABI', '', '', 'Nacional', 2),
(48, 'Jennifer', 'Zambrano', 'VOCAL SEO NUCLEO MANABI', '', '', 'MANABI', 'MANABI', '', '', 'Nacional', 2),
(49, 'Jhonny', 'Torres Parrales', 'VOCAL SEO NUCLEO MANABI', 'johnnyfernando78@hotmail.com', '', 'MANABI', 'MANABI', '', '', 'Nacional', 2),
(50, 'Alex', 'Torres Moreira', 'VOCAL SEO NUCLEO MANABI', '', '', 'MANABI', 'MANABI', '', '', 'Nacional', 2),
(51, 'Willian', 'Andrade Segovia', 'VOCAL SUPLENTE SEO NUCLEO QUITO', 'williamandrades@yahoo.es', '', 'QUITO', 'QUITO', '', '', 'Nacional', 2),
(52, 'Santiago', 'García Alvarado', 'PRESIDENTE SEO NUCLEO CUENCA', 'sagarcialvarado@yahoo.com', '', 'CUENCA', 'CUENCA', '', '', 'Nacional', 2),
(53, 'Patricio', 'Corral Domínguez', 'VICEPRESIDENTE SEO NUCLEO CUENCA', 'patriciocorral@hotmail.com', '', 'CUENCA', 'CUENCA', '', '', 'Nacional', 2),
(54, 'Rocío', 'Murillo', 'TESORERA SEO NUCLEO CUENCA', 'rocimurillo4@yahoo.com.mx', '', 'CUENCA', 'CUENCA', '', '', 'Nacional', 2),
(55, 'Andrés', 'Malo Valdivieso', 'VOCAL SEO NUCLEO CUENCA', 'andresmalo@yahoo.com', '', 'CUENCA', 'CUENCA', '', '', 'Nacional', 2),
(56, 'Esteban', 'León', 'VOCAL SEO NUCLEO CUENCA', 'eleon35@yahoo.com', '', 'CUENCA', 'CUENCA', '', '', 'Nacional', 2),
(57, 'Isabel ', 'León María', 'VOCAL SEO NUCLEO CUENCA', 'marizaleon@yahoo.com', '', 'CUENCA', 'CUENCA', '', '', 'Nacional', 2),
(58, 'Edwin', 'Ceballos Barrera', 'PRESIDENTE SEO NUCLEO QUITO', 'ecevallos@punto.net.ecyahoo.com', '', 'QUITO', 'QUITO', '', '', 'Nacional', 2),
(59, 'Nixón', 'Cevallos', 'VOCAL SUPLENTE SEO NACIONAL', 'ncevallo@hotmail.com', '', 'GUAYAQUIL', '', '', '', 'Nacional', 2),
(60, 'Rosa Yessenia', 'Chavez Franco', '', 'yessi_ch@yahoo.es', '', 'MANABI', '', '', '', 'Nacional', 2),
(61, 'Guillermo', 'Paulson Vernaza', 'PRESIDENTE SEO NACIONAL', 'gpaulsonv@gmail.com', '', 'GUAYAQUIL', '', '', '', 'Nacional', 2),
(62, 'Jorge', 'Moncayo Cervantes', 'VICEPRESIDENTE SEO NACIONAL', 'jmoncayo@autlook.com', '', 'GUAYAQUIL', '', '', '', 'Nacional', 2),
(63, 'Mayra', 'Santacruz Maridueña', 'TESORERA SEO NACIONAL', 'mpsantacruz@yahoo.es', '', 'GUAYAQUIL', '', '', '', 'Nacional', 2),
(64, 'Antonio', 'Jurado Bambino', 'VOCAL PRINCIPAL SEO NACIONAL ', 'antonio_jurado@yahoo.com', '', 'GUAYAQUIL', '', '', '', 'Nacional', 2),
(65, 'Salvador', 'Encalada Orellana', 'VOCAL PRINCIPAL SEO NACIONAL ', 'gladys_chavez_v@hotmail.com', '', 'GUAYAQUIL', '', '', '', 'Nacional', 2),
(66, 'Francisco', 'Plaza Bohorquez', 'VOCAL PRINCIPAL SEO NACIONAL ', 'fplazab@yahoo.es', '', 'GUAYAQUIL', '', '', '', 'Nacional', 2),
(67, 'Dennis', 'Layana Moreno', 'VOCAL SUPLENTE SEO NACIONAL', 'dennis_willi@hotmail.com', '', 'GUAYAQUIL', '', '', '', 'Nacional', 2),
(68, 'Manuel	', 'Contreras Rojas', '', 'mcontreras@solcamed.ec', '', 'GUAYAQUIL', '', '', '', 'Nacional', 2),
(69, 'Santiago', 'Contreras Villavicencio', 'INVITADO', 'jscontrerasv@hotmail.com', '', 'GUAYAQUIL', '', '', '', 'Nacional', 2),
(70, 'Jorge', 'Coronel Jimenez', '', 'drjacj@hotmail.com', '', 'GUAYAQUIL', '', '', '', 'Nacional', 2),
(71, 'Xavier', 'Delgado Camba', '', 'xadelca@hotmail.com', '', 'GUAYAQUIL', '', '', '', 'Nacional', 2),
(72, 'José', 'Encalada Orellana', '', 'J_encalada_o@yahoo.com', '', 'GUAYAQUIL', '', '', '', 'Nacional', 2),
(73, 'Ruth', 'Engracia Vivanco', 'INVITADO', 'rengracia@yahoo.com.ar', '', 'GUAYAQUIL', '', '', '', 'Nacional', 2),
(74, 'Luis', 'Espín Custodio', 'INVITADO', 'lespin@solca.med.ec', '', 'GUAYAQUIL', '', '', '', 'Nacional', 2),
(75, 'Roberto	', 'Falquez Mata', '', 'rfalquez@solca.med.ec', '', 'GUAYAQUIL', '', '', '', 'Nacional', 2),
(76, 'Carlos', 'Freire Alprecht', '', 'alprechtcef@hotmail.com', '', 'GUAYAQUIL', '', '', '', 'Nacional', 2),
(77, 'Elizabeth', 'Gamarra', 'INVITADO', 'elizabethgamarra2003@yahoo.com', '', 'GUAYAQUIL', '', '', '', 'Nacional', 2),
(78, 'César', 'García Cornejo', '', 'garcacornejo@yahoo.es', '', 'GUAYAQUIL', '', '', '', 'Nacional', 2),
(79, 'Katherine', 'García Matamoros', 'SECRETARIA SEO NACIONAL', 'ivanakgm@hotmail.com', '', 'GUAYAQUIL', '', '', '', 'Nacional', 2),
(80, 'Victoria', 'Gordillo', 'INVITADO', 'vikygordillom@yahoo.com', '', 'GUAYAQUIL', '', '', '', 'Nacional', 2),
(81, 'Peter', 'Grijalva Guerrero', '', 'petergrijalva@yahoo.com', '', 'GUAYAQUIL', '', '', '', 'Nacional', 2),
(82, 'Julio', 'Guillén', 'INVITADO', 'drjguillem@yahoo.com', '', 'GUAYAQUIL', '', '', '', 'Nacional', 2),
(83, 'Jorge', 'Jiménez Barahona', '', 'jjimenezbarahona@yahoo.com', '', 'GUAYAQUIL', '', '', '', 'Nacional', 2),
(84, 'Felipe Xavier', 'Kon Jara', 'INVITADO', 'xavierkon@solcamanabi.org', '', 'MANABI', '', '', '', 'Nacional', 2),
(85, 'César', 'León García', '', 'oncomedelg@hotmail.com', '', 'GUAYAQUIL', '', '', '', 'Nacional', 2),
(86, 'Leone', 'Pignataro Mario', '', 'mleone18@hotmail.com', '', 'GUAYAQUIL', '', '', '', 'Nacional', 2),
(87, 'Manuel', 'Loor Suarez', '', 'manuelloor74@hotmail.com', '', 'MANABI', '', '', '', 'Nacional', 2),
(88, 'Allan', 'López Palomares', '', 'allanlopezp@hotmail.com', '', 'GUAYAQUIL', '', '', '', 'Nacional', 2),
(89, 'Bella', 'Maldonado', '', 'maldonadoing81@gmail.com', '', 'GUAYAQUIL', '', '', '', 'Nacional', 2),
(90, 'Carmen', 'Mendoza Narea', 'INVITADO', 'carlexmen@yahoo.com.ar', '', 'GUAYAQUIL', '', '', '', 'Nacional', 2),
(91, 'Guido', 'Panchana Eguez', 'INVITADO', 'panchana@gye.satnet.net', '', 'GUAYAQUIL', '', '', '', 'Nacional', 2),
(92, 'Marcos', 'Parra', '', '', '', 'MACHALA', '', '', '', 'Nacional', 2),
(93, 'Luis', 'Péndola Gómez', 'INVITADO', 'lpendola@hotmail.com', '', 'GUAYAQUIL', '', '', '', 'Nacional', 2),
(94, 'Erick', 'Ponce Ocaña', '', 'erick15ponce@hotmail.com', '', 'MANABI', '', '', '', 'Nacional', 2),
(95, 'Katty', 'Posligua León', '', 'Kposligua19@hotmail.com', '', 'GUAYAQUIL', '', '', '', 'Nacional', 2),
(96, 'Marcel', 'Pozo Palacio', '', 'ytamayol@gye.satnet.net', '', 'GUAYAQUIL', '', '', '', 'Nacional', 2),
(97, 'Juan	', 'Ramirez', 'INVITADO', '', '', 'GUAYAQUIL', '', '', '', 'Nacional', 2),
(98, 'Glenda', 'Ramos Martinez', '', 'gramosmartinez@hotmail.com', '', 'GUAYAQUIL', '', '', '', 'Nacional', 2),
(99, 'Alba	', 'Rhor Alvarado', 'INVITADO', 'arhor65@hotmail.com', '', 'GUAYAQUIL', '', '', '', 'Nacional', 2),
(100, 'Mauricio		', 'Riofrío Riofrío', 'INVITADO', 'mriofrio@hotmail.com', '', 'GUAYAQUIL', '', '', '', 'Nacional', 2),
(101, 'Nery			', 'Rivadeneira Santana', '', 'nerys@gye.satnet.net', '', 'GUAYAQUIL', '', '', '', 'Nacional', 2),
(102, 'Patricia		', 'Rizzo Palma', 'INVITADO', 'prizzomoncayo@gmail.com', '', 'GUAYAQUIL', '', '', '', 'Nacional', 2),
(103, 'Erika', 'Ruilova', '', '', '', 'MACHALA', '', '', '', 'Nacional', 2),
(104, 'Juan Carlos', 'Ruiz Cabezas', 'INVITADO', 'ec-jruizc@hotmail.com', '', 'GUAYAQUIL', '', '', '', 'Nacional', 2),
(105, 'Hollwuin', 'Solórzano Morales', 'INVITADO', 'h_solorzano_m@yahoo.com', '', 'GUAYAQUIL', '', '', '', 'Nacional', 2),
(106, 'Rosa		', 'Villacis', '', '', '', 'MACHALA', '', '', '', 'Nacional', 2),
(107, 'Angel	', 'Vinces Gilces', '', 'angelvingil@hotmail.com', '', 'MANABI', '', '', '', 'Nacional', 2),
(108, 'Melva', 'Alarcón', '', 'mecha_65@hotmail.com', '', 'QUITO', '', '', '', 'Nacional', 2),
(109, 'Marcelo', 'Alvarez Molina', 'INVITADO', 'marceloalvarezmolina@hotmail.com', '', 'QUITO', '', '', '', 'Nacional', 2),
(110, 'Williams', 'Arias', '', 'willariasmx@yahoo.com.mx', '', 'QUITO', '', '', '', 'Nacional', 2),
(111, 'Edwin', 'Ávalos', '', 'edwin.avalos@hotmail.es', '', 'QUITO', '', '', '', 'Nacional', 2),
(112, 'Amparo', 'Basantes Pinos', 'INVITADO', 'angelesb@uio.satnet.net', '', 'QUITO', '', '', '', 'Nacional', 2),
(113, 'Andrea', 'Bautista', '', 'aebautistar@hotmail.com', '', 'QUITO', '', '', '', 'Nacional', 2),
(114, 'Gil', 'Bermeo', '', 'gbermeo@ecnet.ec', '', 'QUITO', '', '', '', 'Nacional', 2),
(115, 'Mónica		', 'Cachimuel', '', 'menamonidj@yahoo.com.ar', '', 'QUITO', '', '', '', 'Nacional', 2),
(116, 'Myriam	', 'Castro', '', 'marguello@hotmail.com', '', 'QUITO', '', '', '', 'Nacional', 2),
(117, 'Fernando', 'Checa Ron', '', 'checafernando@yahoo.es', '', 'QUITO', '', '', '', 'Nacional', 2),
(118, 'Eulalia', 'Chusino', 'INVITADO', '', '', 'QUITO', '', '', '', 'Nacional', 2),
(119, 'Carlos', 'Eugenio', '', 'carloseugenio1974@yahoo.com', '', 'QUITO', '', '', '', 'Nacional', 2),
(120, 'Patricio', 'Flores', '', 'pgavilanez@hotmail.com', '', 'QUITO', '', '', '', 'Nacional', 2),
(121, 'Carola', 'Guerrero', '', 'carolaguerrerov@hotmail.com', '', 'QUITO', '', '', '', 'Nacional', 2),
(122, 'Paulina', 'Irigoyen', 'INVITADO', 'polis_cyt@hotmail.com', '', 'QUITO', '', '', '', 'Nacional', 2),
(123, 'Milton	', 'Jara', '', 'miltonjara@hotmail.com', '', 'QUITO', '', '', '', 'Nacional', 2),
(124, 'Carolina', 'Jaramillo', '', 'ccjaramillo@hotmail.com', '', 'QUITO', '', '', '', 'Nacional', 2),
(125, 'Bárbara', 'León', '', 'barbaralem@yahoo.com', '', 'QUITO', '', '', '', 'Nacional', 2),
(126, 'Hugo Hernán', 'Lupera Pazmiño', 'INVITADO', 'hlupera@gmail.com', '', 'QUITO', '', '', '', 'Nacional', 2),
(127, 'Carlos', 'Mejía', '', 'drcmejiao@hotmail.com', '', 'QUITO', '', '', '', 'Nacional', 2),
(128, 'Andrea', 'Moreno', 'INVITADO', 'andypmo77@yahoo.com', '', 'QUITO', '', '', '', 'Nacional', 2),
(129, 'Mushtaq', 'Ahmad', 'VOCAL SUPLENTE SEO NUCLEO QUITO', 'oncowali@yahoo.es', '', 'QUITO', 'QUITO', '', '', 'Nacional', 2),
(130, 'Vinicio', 'Navas', '', 'syepeznavas@andinanet.net', '', 'QUITO', '', '', '', 'Nacional', 2),
(131, 'Karina		', 'Nuñez', '', 'karinanunez14@hotmail.com', '', 'QUITO', '', '', '', 'Nacional', 2),
(132, 'Cristina', 'Nuñez', '', 'gcnsdm@yahoo.es', '', 'QUITO', '', '', '', 'Nacional', 2),
(133, 'Eduardo', 'Pardo', '', 'drleonpardo@yahoo.com', '', 'QUITO', '', '', '', 'Nacional', 2),
(134, 'Raúl', 'Pineda', 'INVITADO', 'raulpinedaochoa@hotmail.com', '', '', '', '', '', 'Nacional', 2),
(135, 'Diego', 'Pinto', '', 'drpintof@yahoo.com', '', 'QUITO', '', '', '', 'Nacional', 2),
(136, 'Mario	', 'Riofrio Villavicencio', 'INVITADO', 'marioriofrio@hotmail.com', '', 'QUITO', '', '', '', 'Nacional', 2),
(137, 'Jimmy', 'Rocas', '', 'jirocas@yahoo.es', '', 'QUITO', '', '', '', 'Nacional', 2),
(138, 'Edgar', 'Rodríguez', 'INVITADO', 'edgarodriguezp1977@outlook.com', '', 'QUITO', '', '', '', 'Nacional', 2),
(139, 'Miguel', 'Rueda', '', 'drrueda72@hotmail.com', '', 'QUITO', '', '', '', 'Nacional', 2),
(140, 'Luis', 'Ruiz Llerena', 'INVITADO', 'luisruizllerena@hotmail.com', '', 'QUITO', '', '', '', 'Nacional', 2),
(141, 'Carmen', 'Sánchez Ávila', '', 'carmitas8@hotmail.com', '', 'QUITO', '', '', '', 'Nacional', 2),
(142, 'Nelson', 'Segovia', '', 'negrosegovia@hotmail.com', '', 'QUITO', '', '', '', 'Nacional', 2),
(143, 'Tania', 'Soria Samaniego', 'INVITADO', 'tsoria21@hotmail.com', '', 'QUITO', '', '', '', 'Nacional', 2),
(144, 'Ricardo', 'Tixi Ramírez', '', 'rtixioncologo@hotmail.com', '', 'QUITO', '', '', '', 'Nacional', 2),
(145, 'Alex', 'Vásconez García', '', 'avasconez@hotmail.com', '', 'QUITO', '', '', '', 'Nacional', 2),
(146, 'Tatiana', 'Vinueza', '', 'taticuvinueza@gmail.com', '', 'QUITO', '', '', '', 'Nacional', 2),
(147, 'Napoleón', 'Yánez', '', 'npyanez24@gmail.com', '', 'QUITO', '', '', '', 'Nacional', 2),
(148, 'Byron', 'Álvarez', '', 'bayalvarez@yahoo.com', '', 'CUENCA', '', '', '', 'Nacional', 2),
(149, 'Marx', 'Bravo', 'INVITADO', 'marxbravo@hotmail.com', '', 'CUENCA', '', '', '', 'Nacional', 2),
(150, 'José', 'Medina Cuello', '', 'jmedinacoello@hotmail.com', '', 'CUENCA', '', '', '', 'Nacional', 2),
(151, 'Felipe', 'Vásquez', '', 'svp.oncologo@hotmail.com', '', 'CUENCA', '', '', '', 'Nacional', 2),
(152, 'Nardo', 'Vivar', '', 'natisv52@hotmail.com', '', 'CUENCA', '', '', '', 'Nacional', 2);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
