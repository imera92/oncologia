<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
        $this->load->database();
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('grocery_CRUD');
        $this->load->model('SecurityUser');
        date_default_timezone_set("America/Guayaquil");
	}

	public function login() {
        if($this->securityCheckAdmin()){
            redirect("admin/index");
        }else{
            $titulo = "SEO::Admin";
            $dataHeader['titlePage'] = $titulo;

            $this->load->view('admin/header', $dataHeader);
            $this->load->view('admin/login');
            $this->load->view('admin/footer');
        }
    }

    public function index() {
    	if ($this->securityCheckAdmin()) {
    		$titulo = "SEO::Admin - Inicio";

    		$dataHeader['titlePage'] = $titulo;

    		$this->load->view('admin/header', $dataHeader);
    		$this->load->view('admin/lat-menu');
    		$this->load->view('admin/index');
    		$this->load->view('admin/footer');
    	} else {
    		redirect("admin/login");
    	}
    }

	public function logout() {
        if($this->securityCheckAdmin()){
            $securityUser = new SecurityUser();
            $securityUser->logout();
            redirect("admin/login");
        }else{
            redirect("admin/login");
        }
    }

	public function auth() {
        $user = $this->input->post("user");
        $password = $this->input->post("password");

        $securityUser = new SecurityUser();
        $securityUser->login_admin($user, $password);

        if($this->session->userdata('user') != "" && $this->session->userdata('tipo') == "admin"){
            redirect("admin/index");
        }else{
            redirect("admin/login");
        }
    }

    public function socios() {
    	if($this->securityCheckAdmin()){
			$titulo = "Socios";

			$crud = new grocery_CRUD();
			$crud->set_table("socios");
			$crud->set_subject($titulo);

			$crud->display_as('nombre', 'Nombre');
			$crud->display_as('apellido', 'Apellido');
			$crud->display_as('correo', 'Email');
			$crud->display_as('posicion', 'Posición');
			$crud->display_as('foto', 'Foto');
			$crud->display_as('ciudad', 'Ciudad');
			$crud->display_as('nucleo', 'Núcleo');
			$crud->display_as('telefono', 'Teléfono');
			$crud->display_as('celular', 'Celular');
			$crud->display_as('localizacion', 'Localización');
			$crud->display_as('destacado', 'Destacado');
			$crud->field_type('nucleo', 'dropdown', array(
				'Guayas' => 'Guayas',
				'Manabi' => 'Manabi',
				'Azuay' => 'Azuay',
				'Pichincha' => 'Pichincha'
			));
			$crud->field_type('localizacion', 'dropdown', array(
				'Nacional' => 'Nacional',
				'Internacional' => 'Internacional'
			));
			$crud->field_type('destacado', 'dropdown', array(
				'1' => 'Sí',
				'2' => 'No'
			));
			$crud->set_field_upload('foto', 'assets/uploads/socios');
			$crud->columns('foto', 'nombre', 'apellido', 'posicion', 'localizacion', 'nucleo', 'ciudad', 'correo', 'telefono', 'celular', 'destacado');
			$crud->fields('foto', 'nombre', 'apellido', 'posicion', 'localizacion', 'nucleo', 'ciudad', 'correo', 'telefono', 'celular', 'destacado');
			$crud->required_fields('nombre', 'apellido', 'correo', 'posicion', 'ciudad', 'localizacion', 'destacado');
			$crud->unset_export();
	        $crud->unset_print();
            $crud->set_language("spanish");
            // $crud->callback_after_insert(array($this,'defaultImgCallback'));
            // $crud->callback_after_update(array($this,'defaultImgCallback'));
	        $output = $crud->render();

	        $dataHeader['titlePage'] = 'SEO::Admin - Socios';
	        $dataHeader['titleCRUD'] = $titulo;
	        $dataHeader['css_files'] = $output->css_files;
	        $dataFooter['js_files'] = $output->js_files;
	        
	        $data['header'] = $this->load->view('admin/header', $dataHeader);
	        $data['menu'] = $this->load->view('admin/lat-menu');
	        $data['content'] = $this->load->view('admin/blank', (array)$output);
	        $data['footer'] = $this->load->view('admin/footer-gc', $dataFooter);
		}else{
			redirect("admin/login");
		}
    }

    public function eventos() {
    	if($this->securityCheckAdmin()){
			$titulo = "Eventos";

			$crud = new grocery_CRUD();
			$crud->set_table("eventos");
			$crud->set_subject($titulo);

			$crud->display_as('nombre', 'Nombre');
			$crud->display_as('descripcion', 'Descripción');
			$crud->display_as('tipo', 'Tipo');
			$crud->display_as('localizacion', 'Localización');
			$crud->display_as('contacto', 'Contacto');
			$crud->display_as('inicio', 'Inicio');
			$crud->display_as('fin', 'Fin');
			$crud->field_type('localizacion', 'dropdown', array(
				'azuay' => 'Azuay',
				'guayas' => 'Guayas',
				'manabi' => 'Manabí',
				'pichincha' => 'Pichincha',
				// 'Nacional' => 'Nacional',
				'Internacional' => 'Internacional'
			));
			$crud->field_type('descripcion', 'text');
			$crud->field_type('inicio', 'date');
			$crud->field_type('fin', 'date');
			$crud->columns('nombre', 'tipo', 'descripcion', 'localizacion', 'inicio', 'fin', 'contacto');
			$crud->fields('nombre', 'tipo', 'descripcion', 'localizacion', 'inicio', 'fin', 'contacto');
			$crud->required_fields('nombre', 'tipo', 'descripcion', 'localizacion', 'inicio', 'fin', 'contacto');
			$crud->unset_export();
	        $crud->unset_print();
	        $crud->unset_texteditor('descripcion', 'full_text');
            $crud->set_language("spanish");
	        $output = $crud->render();

	        $dataHeader['titlePage'] = 'SEO::Admin - Eventos';
	        $dataHeader['titleCRUD'] = $titulo;
	        $dataHeader['css_files'] = $output->css_files;
	        $dataFooter['js_files'] = $output->js_files;
	        
	        $data['header'] = $this->load->view('admin/header', $dataHeader);
	        $data['menu'] = $this->load->view('admin/lat-menu');
	        $data['content'] = $this->load->view('admin/blank', (array)$output);
	        $data['footer'] = $this->load->view('admin/footer-gc', $dataFooter);
		}else{
			redirect("admin/login");
		}
    }

    public function opiniones() {
    	if($this->securityCheckAdmin()){
			$titulo = "Opiniones";

			$crud = new grocery_CRUD();
			$crud->set_table("opiniones");
			$crud->set_subject($titulo);

			$crud->display_as('titulo', 'Título');
			$crud->display_as('foto', 'Foto del Doctor');
			$crud->display_as('descripcion', 'Texto');
			$crud->display_as('doctor', 'Nombre del Doctor');
			$crud->display_as('fecha', 'Fecha de Publicación');
			$crud->display_as('especializacion', 'Especialización del Doctor');
			$crud->set_field_upload('foto', 'assets/uploads');
			$crud->field_type('descripcion', 'text');
			$crud->field_type('fecha', 'date');
			$crud->columns('titulo', 'descripcion', 'doctor', 'especializacion', 'foto', 'fecha');
			$crud->fields('titulo', 'descripcion', 'doctor', 'especializacion', 'foto', 'fecha');
			$crud->required_fields('titulo', 'descripcion', 'doctor', 'especializacion', 'foto', 'fecha');
			$crud->unset_export();
	        $crud->unset_print();
            $crud->set_language("spanish");
	        $output = $crud->render();

	        $dataHeader['titlePage'] = 'SEO::Admin - Nuestros Socios Opinan';
	        $dataHeader['titleCRUD'] = $titulo;
	        $dataHeader['css_files'] = $output->css_files;
	        $dataFooter['js_files'] = $output->js_files;
	        
	        $data['header'] = $this->load->view('admin/header', $dataHeader);
	        $data['menu'] = $this->load->view('admin/lat-menu');
	        $data['content'] = $this->load->view('admin/blank', (array)$output);
	        $data['footer'] = $this->load->view('admin/footer-gc', $dataFooter);
		}else{
			redirect("admin/login");
		}
    }

    public function noticias() {
    	if($this->securityCheckAdmin()){
			$titulo = "Noticias";

			$crud = new grocery_CRUD();
			$crud->set_table("noticias");
			$crud->set_subject($titulo);

			$crud->display_as('titulo', 'Título');
			$crud->display_as('imagen', 'Imagen');
			$crud->display_as('texto', 'Texto');
			$crud->field_type('texto', 'text');
			$crud->set_field_upload('imagen', 'assets/uploads/noticias');
			$crud->columns('titulo', 'imagen', 'texto');
			$crud->fields('titulo', 'imagen', 'texto');
			$crud->required_fields('titulo', 'texto');
			$crud->callback_after_insert(array($this,'currentDateTimeCallback'));
			$crud->unset_export();
	        $crud->unset_print();
            $crud->set_language("spanish");
	        $output = $crud->render();

	        $dataHeader['titlePage'] = 'SEO::Admin - Noticias';
	        $dataHeader['titleCRUD'] = $titulo;
	        $dataHeader['css_files'] = $output->css_files;
	        $dataFooter['js_files'] = $output->js_files;
	        
	        $data['header'] = $this->load->view('admin/header', $dataHeader);
	        $data['menu'] = $this->load->view('admin/lat-menu');
	        $data['content'] = $this->load->view('admin/blank', (array)$output);
	        $data['footer'] = $this->load->view('admin/footer-gc', $dataFooter);
		}else{
			redirect("admin/login");
		}
    }

    public function carousel () {
    	if($this->securityCheckAdmin()) {
    		$titulo = "Imágenes del Carousel";

			$crud = new grocery_CRUD();
			$crud->set_table("imagenescarousel");
			$crud->set_subject($titulo);

			$crud->display_as('imagen', 'Imagen');
			$crud->display_as('link', 'Link');
			$crud->display_as('alt', 'Texto de la imagen');
			$crud->display_as('estado', 'Estado');
			$crud->field_type('estado', 'dropdown', array(
				'1' => 'Activo',
				'2' => 'Inactivo'
			));
			$crud->set_field_upload('imagen', 'assets/uploads/carousel');
			$crud->columns('imagen', 'link', 'alt', 'estado');
			$crud->fields('imagen', 'link', 'alt', 'estado');
			$crud->required_fields('imagen', 'estado');
			$crud->unset_export();
	        $crud->unset_print();
            $crud->set_language("spanish");
	        $output = $crud->render();

	        $dataHeader['titlePage'] = 'SEO::Admin - Carousel';
	        $dataHeader['titleCRUD'] = $titulo;
	        $dataHeader['css_files'] = $output->css_files;
	        $dataFooter['js_files'] = $output->js_files;
	        
	        $data['header'] = $this->load->view('admin/header', $dataHeader);
	        $data['menu'] = $this->load->view('admin/lat-menu');
	        $data['content'] = $this->load->view('admin/blank', (array)$output);
	        $data['footer'] = $this->load->view('admin/footer-gc', $dataFooter);
    	} else {
    		redirect("admin/login");
    	}
    }

    public function archivos () {
    	if($this->securityCheckAdmin()) {
    		$titulo = "Archivos";

			$crud = new grocery_CRUD();
			$crud->set_table("archivoseventos");
			$crud->set_subject($titulo);

			$crud->display_as('evento', 'Evento');
			$crud->display_as('archivo', 'Archivo');
			$crud->set_field_upload('archivo', 'assets/uploads/files');
			$crud->columns('evento', 'archivo');
			$crud->fields('evento', 'archivo');
			$crud->required_fields('evento', 'archivo');
			$crud->set_relation('evento','eventos','{nombre} - {inicio}', 'inicio < curdate()', 'inicio ASC');
			$crud->unset_export();
	        $crud->unset_print();
            $crud->set_language("spanish");
	        $output = $crud->render();

	        $dataHeader['titlePage'] = 'SEO::Admin - Archivos';
	        $dataHeader['titleCRUD'] = $titulo;
	        $dataHeader['css_files'] = $output->css_files;
	        $dataFooter['js_files'] = $output->js_files;
	        
	        $data['header'] = $this->load->view('admin/header', $dataHeader);
	        $data['menu'] = $this->load->view('admin/lat-menu');
	        $data['content'] = $this->load->view('admin/blank', (array)$output);
	        $data['footer'] = $this->load->view('admin/footer-gc', $dataFooter);
    	} else {
    		redirect("admin/login");
    	}
    }

    public function links_mbe () {
    	if($this->securityCheckAdmin()) {
    		$titulo = 'Links de MBE (Medicina Basada en Evidencia)';
    		$titulo_crud = 'Link de MBE';

			$crud = new grocery_CRUD();
			$crud->set_table('links_mbe');
			$crud->set_subject($titulo_crud);
			$crud->columns('titulo', 'link', 'archivo');
			$crud->fields('titulo', 'link', 'archivo');
			$crud->required_fields('titulo');
			$crud->display_as('titulo', 'Título');
			$crud->set_field_upload('archivo', 'assets/uploads/files/links_de_interes/mbe');
			$crud->set_rules('archivo', 'Archivo', 'callback_upload_file_check');
			$crud->unset_export();
	        $crud->unset_print();
            $crud->set_language('spanish');
	        $output = $crud->render();

	        $dataHeader['titlePage'] = 'SEO::Admin - Links de MBE';
	        $dataHeader['titleCRUD'] = $titulo;
	        $dataHeader['css_files'] = $output->css_files;
	        $dataFooter['js_files'] = $output->js_files;
	        
	        $data['header'] = $this->load->view('admin/header', $dataHeader);
	        $data['menu'] = $this->load->view('admin/lat-menu');
	        $data['content'] = $this->load->view('admin/blank', (array)$output);
	        $data['footer'] = $this->load->view('admin/footer-gc', $dataFooter);
    	} else {
    		redirect('admin/login');
    	}
    }

     public function links_oncologia () {
    	if($this->securityCheckAdmin()) {
    		$titulo = 'Links de Oncología';
    		$titulo_crud = 'Link de Oncología';

			$crud = new grocery_CRUD();
			$crud->set_table('links_oncologia');
			$crud->set_subject($titulo_crud);
			$crud->columns('titulo', 'link', 'archivo');
			$crud->fields('titulo', 'link', 'archivo');
			$crud->required_fields('titulo');
			$crud->display_as('titulo', 'Título');
			$crud->set_field_upload('archivo', 'assets/uploads/files/links_de_interes/oncologia');
			$crud->set_rules('archivo', 'Archivo', 'callback_upload_file_check');
			$crud->unset_export();
	        $crud->unset_print();
            $crud->set_language('spanish');
	        $output = $crud->render();

	        $dataHeader['titlePage'] = 'SEO::Admin - Links de Oncología';
	        $dataHeader['titleCRUD'] = $titulo;
	        $dataHeader['css_files'] = $output->css_files;
	        $dataFooter['js_files'] = $output->js_files;
	        
	        $data['header'] = $this->load->view('admin/header', $dataHeader);
	        $data['menu'] = $this->load->view('admin/lat-menu');
	        $data['content'] = $this->load->view('admin/blank', (array)$output);
	        $data['footer'] = $this->load->view('admin/footer-gc', $dataFooter);
    	} else {
    		redirect('admin/login');
    	}
    }

    function securityCheckAdmin() {
        $securityUser = new SecurityUser();
        $usuario = $this->session->userdata('user');
        if($usuario == ""){
            return false;
        }else{
            if ($this->session->userdata('tipo') == "admin") {
                return true;
            }else{
                $securityUser->logout();
                return false;
            }
        }
    }

    function defaultImgCallback ($post_array, $primary_key) {
    	$socio = $this->db->get_where('socios', array('id' => $primary_key))->result_array();
    	if ($socio[0]['foto'] == '') {
    		$socio[0]['foto'] = 'default.png';
    	}
    	$this->db->replace('socios', $socio[0]);
    }

    function currentDateTimeCallback ($post_array, $primary_key) {
    	$post = $this->db->get_where('noticias', array('id' => $primary_key))->result_array();
    	$post[0]['fecha'] = date("Y-m-d h:i:sa");
    	$this->db->replace('noticias', $post[0]);
    }

    function upload_file_check($data)
    {
    	if (!empty($data) && !empty($this->input->post('link'))) {
    		$this->form_validation->set_message('upload_file_check', 'No puede ingresar un link y un archivo al mismo tiempo.');
    		return false;
    	} else {
    		return true;
    	}
    }
}
