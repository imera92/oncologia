<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Web extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
        $this->load->helper('url');
        $this->load->database();
        date_default_timezone_set("America/Guayaquil");
	}

	public function index()
	{		
		$titulo = 'SEO';
		$dataHeader['titlePage'] = $titulo;

		$query = $this->db->query('select * from eventos where inicio >= curdate() and localizacion = "Nacional" order by inicio asc limit 3');
		$dataBody['eventosNacionales'] = $query->result_array();
		$query = $this->db->query('select * from eventos where inicio >= curdate() and localizacion = "Internacional" order by inicio asc limit 3');
		$dataBody['eventosInternacionales'] = $query->result_array();
		$this->db->select('*');
		$this->db->from('imagenescarousel');
		$this->db->where('estado', 1);
		$this->db->order_by('id', 'DESC');
		// $dataBody['imagenesCarousel'] = $this->db->get_where('imagenescarousel', array('estado' => 1))->result_array();
		$dataBody['imagenesCarousel'] = $this->db->get()->result_array();

		$this->load->view('web/header', $dataHeader);
		$this->load->view('web/home', $dataBody);
		$this->load->view('web/footer');
	}

	public function quienes()
	{
		$titulo = 'SEO';
		$dataHeader['titlePage'] = $titulo;

		$dataBody['imagenesCarousel'] = $this->db->get_where('imagenescarousel', array('estado' => 1))->result_array();

		$this->load->view('web/header', $dataHeader);
		$this->load->view('web/quienes', $dataBody);
		$this->load->view('web/footer');
	}

	public function opinion($pagina=1)
	{
		$titulo = 'SEO';
		$dataHeader['titlePage'] = $titulo;
		$this->load->view('web/header', $dataHeader);

		// Calculamos la cantidad de paginas que debe haber en base al numero de opiniones
		$query = $this->db->query('select count(*) as cantidad from opiniones');
		$result = $query->result_array();
		$cantidadDePaginas = $result[0]['cantidad'];

		// Validamos que las paginas a mostrar no superen las 10
		if ($cantidadDePaginas > 10) {
			$cantidadDePaginas = 10;
		}

		// Validamos que no se solicite una pagina inexistente
		if ($pagina > $cantidadDePaginas || $pagina <= 0) {
			$pagina = 1;
		}

		// Obtenemos la opinion correspondiente a la pagina solicitada
		$indice = ($pagina - 1);
		$query = $this->db->query('select * from opiniones order by fecha desc limit 1 offset ' . $indice);
		$result = $query->result_array();

		$dataBody['paginaSolicitada'] = $pagina;
		$dataBody['cantidadDePaginas'] = $cantidadDePaginas;
		$dataBody['opinion'] = $result[0];

		// Cargamos los eventos que se muestran en la tabla
		$query = $this->db->query('select * from eventos where inicio >= curdate() and localizacion = "Nacional" order by inicio asc limit 3');
		$dataBody['eventosNacionales'] = $query->result_array();
		$query = $this->db->query('select * from eventos where inicio >= curdate() and localizacion = "Internacional" order by inicio asc limit 3');
		$dataBody['eventosInternacionales'] = $query->result_array();

		$this->load->view('web/opinion', $dataBody);
		$this->load->view('web/footer');
	} 

	public function links()
	{
		$titulo = 'SEO';
		$dataHeader['titlePage'] = $titulo;

		$this->load->view('web/header', $dataHeader);

		$query = $this->db->query('select * from eventos where inicio >= curdate() and localizacion = "Nacional" order by inicio asc limit 3');
		$dataBody['eventosNacionales'] = $query->result_array();
		$query = $this->db->query('select * from eventos where inicio >= curdate() and localizacion = "Internacional" order by inicio asc limit 3');
		$dataBody['eventosInternacionales'] = $query->result_array();

		$this->load->view('web/links', $dataBody);
		$this->load->view('web/footer');
	}

	public function mbe()
	{
		$titulo = 'SEO';
		$dataHeader['titlePage'] = $titulo;

		$this->load->view('web/header', $dataHeader);

		$dataBody['links'] = $this->db->get('links_mbe')->result();

		$query = $this->db->query('select * from eventos where inicio >= curdate() and localizacion = "Nacional" order by inicio asc limit 3');
		$dataBody['eventosNacionales'] = $query->result_array();
		$query = $this->db->query('select * from eventos where inicio >= curdate() and localizacion = "Internacional" order by inicio asc limit 3');
		$dataBody['eventosInternacionales'] = $query->result_array();

		$this->load->view('web/mbe', $dataBody);
		$this->load->view('web/footer');
	}

	public function oncologia()
	{
		$titulo = 'SEO';
		$dataHeader['titlePage'] = $titulo;

		$this->load->view('web/header', $dataHeader);

		$dataBody['links'] = $this->db->get('links_oncologia')->result();

		$query = $this->db->query('select * from eventos where inicio >= curdate() and localizacion = "Nacional" order by inicio asc limit 3');
		$dataBody['eventosNacionales'] = $query->result_array();
		$query = $this->db->query('select * from eventos where inicio >= curdate() and localizacion = "Internacional" order by inicio asc limit 3');
		$dataBody['eventosInternacionales'] = $query->result_array();

		$this->load->view('web/oncologia', $dataBody);
		$this->load->view('web/footer');
	}

	public function socios($pagina = '')
	{
		$titulo = 'SEO';
		$dataHeader['titlePage'] = $titulo;

		if (in_array($pagina, range('A', 'Z'))) {
			$query = $this->db->query('select * from socios where localizacion = "Nacional" and apellido like "'.$pagina.'%" order by apellido, nombre');
			$dataBody['sociosNacionales'] = $query->result_array();
			$query = $this->db->query('select * from socios where localizacion = "Internacional" and apellido like "'.$pagina.'%" order by apellido, nombre');
			$dataBody['sociosInternacionales'] = $query->result_array();

			$dataBody['pagina'] = $pagina;
		}elseif ($pagina == '' || !in_array($pagina, range('A', 'Z'))) {
			// Si no solicita el numero de pagina, vamos a mostrar los 18 socios mas recientes

			$sociosNacionales = array();
			$sociosInternacionales = array();

			// Consultamos a la base por los 18 socios mas recientes
			$this->db->select('*');
			$this->db->from('socios');
			$this->db->where('destacado', 1);
			$this->db->order_by('apellido', 'ASC');
			$this->db->limit(18);
			$result = $this->db->get()->result_array();

			// Separamos los socios que tienen foto de los que no tienen foto
			foreach ($result as $index => $value) {
				if ($value['localizacion'] == 'Nacional') {
					array_push($sociosNacionales, $value);
				} else {
					array_push($sociosInternacionales, $value);
				}
			}

			$dataBody['sociosNacionales'] = $sociosNacionales;
			$dataBody['sociosInternacionales'] = $sociosInternacionales;
		}
		
		$query = $this->db->query('select * from eventos where inicio >= curdate() and localizacion = "Nacional" order by inicio asc limit 3');
		$dataBody['eventosNacionales'] = $query->result_array();
		$query = $this->db->query('select * from eventos where inicio >= curdate() and localizacion = "Internacional" order by inicio asc limit 3');
		$dataBody['eventosInternacionales'] = $query->result_array();

		$this->load->view('web/header', $dataHeader);
		$this->load->view('web/socios', $dataBody);
		$this->load->view('web/footer');
	}

	public function eventos($localizacion = '')
	{
		$titulo = 'SEO';
		$dataHeader['titlePage'] = $titulo;

		if ($localizacion == '') {
			$this->db->order_by('inicio', 'DESC');
			$query = $this->db->get('eventos');
		} else {
			// $query = $this->db->get_where('eventos', array('localizacion' => $localizacion));
			$this->db->select('*');
			$this->db->from('eventos');
			$this->db->where('localizacion', $localizacion);
			$this->db->order_by('inicio', 'DESC');
			$query = $this->db->get();
		}

		// $query = $this->db->query('select * from eventos where fin >= curdate() order by inicio asc');
		$dataBody['eventos'] = $query->result_array();

		$this->load->view('web/header', $dataHeader);
		$this->load->view('web/eventos', $dataBody);
		$this->load->view('web/footer');
	}

	public function noticias($num='')
	{
		$titulo = 'SEO';
		$dataHeader['titlePage'] = $titulo;

		$query = $this->db->query('select * from eventos where inicio >= curdate() and localizacion = "Nacional" order by inicio asc limit 3');
		$dataBody['eventosNacionales'] = $query->result_array();
		$query = $this->db->query('select * from eventos where inicio >= curdate() and localizacion = "Internacional" order by inicio asc limit 3');
		$dataBody['eventosInternacionales'] = $query->result_array();

		$this->load->view('web/header', $dataHeader);
		if ($num == '') {
			$dataBody['post'] = $this->db->get('noticias')->last_row()->result_array();
			$this->load->view('web/post', $dataBody);
			$this->load->view('web/footer');
		}else{
			$this->db->select('*');
			$this->db->from('noticias');
			$this->db->where('id<=', $num);
			$this->db->order_by('fecha', 'DESC');
			$this->db->limit(2);
			$dataBody['post'] = $this->db->get()->result_array();
			$this->load->view('web/post', $dataBody);
			$this->load->view('web/footer');
		}
	}

	public function noticiasActuales($pagina = 1)
	{	
		$titulo = 'SEO';
		$dataHeader['titlePage'] = $titulo;

		// Calculamos la cantidad de paginas que debe haber en base al numero de noticias restantes
		$query = $this->db->query('select count(*) as cantidad from noticias');
		$result = $query->result_array();
		$cantidadDePaginas = (int)ceil($result[0]['cantidad'] / 5);

		// Validamos que las paginas a mostrar no superen las 10
		if ($cantidadDePaginas > 10) {
			$cantidadDePaginas = 10;
		}

		// Validamos que no se solicite una pagina de noticias inexistente
		if ($pagina > $cantidadDePaginas) {
			$pagina = 1;
		}

		// Obtenemos las 5 noticias correspondientes a la pagina solicitada
		$indice = ($pagina - 1) * 5;
		$query = $this->db->query('select * from noticias order by fecha desc limit 5 offset ' . $indice);
		$noticias = $query->result_array();

		// Modificamos el texto que se va a mostrar de cada noticia
		foreach ($noticias as $index => $noticia) {
			$noticias[$index]['texto'] = substr($noticia['texto'], 0, 500) . '...';

			$tagsEnTexto = array();
			$tagsCerrados = array();
			$tagsAbiertos = array();
			$tagsAbiertosEnOrden = array();
			$tag = '';
			$charFlag = false;

			// Identificamos todos los tags que fueron abiertos en el texto y los guardamos en tagsEnTexto
			for ($i=0; $i < strlen($noticias[$index]['texto']) ; $i++) {
				if ($noticias[$index]['texto'][$i] == '<') {
					if ($noticias[$index]['texto'][$i+1] != '/') {
						$charFlag = true;
					}
				} elseif ($noticias[$index]['texto'][$i] == '>') {
					if ($tag != '') {
						array_push($tagsEnTexto, $tag);
						$tag = '';
						$charFlag = false;
					}
				}

				if ($charFlag == true) {
					if ($noticias[$index]['texto'][$i] != '<') {
						$tag .= $noticias[$index]['texto'][$i];
					}
				}
			}

			// Identificamos todos los tags que fueron cerrados en el texto y los guardamos en tagsCerrados
			for ($i=0; $i < strlen($noticias[$index]['texto']) ; $i++) {
				if ($noticias[$index]['texto'][$i] == '<') {
					if ($noticias[$index]['texto'][$i+1] == '/') {
						$charFlag = true;
					}
				} elseif ($noticias[$index]['texto'][$i] == '>') {
					if ($tag != '') {
						array_push($tagsCerrados, $tag);
						$tag = '';
						$charFlag = false;
					}
				}

				if ($charFlag == true) {
					if ($noticias[$index]['texto'][$i] != '<' && $noticias[$index]['texto'][$i] != '/') {
						$tag .= $noticias[$index]['texto'][$i];
					}
				}
			}

			// PRUEBA: Imprimimos el array con los tags que fueron abiertos en el texto y el array con los tags que sí se cerraron
			/*print_r($tagsEnTexto);
			print_r($tagsCerrados);*/

			// Confirmamos que los tags que fueron abiertos en el texto tengan su respectivo cierre. De no ser así, los guardamos en el array tagsAbiertos
			foreach ($tagsEnTexto as $index2 => $value) {
				$tagEstaCerrado = false;

				$tagEstaCerrado = array_search($value, $tagsCerrados);

				if ($tagEstaCerrado === false) {
					array_push($tagsAbiertos, $value);
				} else {
					// IMPORTANTE: Este unset nos ayuda cuando un mismo tag fue abierto más de 1 vez en el texto
					unset($tagsCerrados[$tagEstaCerrado]);
				}
			}

			// Reordenamos el array de tags que no tuvieron cierre, para enviarlos al frontend en el orden en que deben de cerrarse
			if (!empty($tagsAbiertos)) {
				$tagsAbiertosEnOrden = array_reverse($tagsAbiertos);
			}

			// PRUEBA: Imprimimos el array de tags que no tuvieron cierre (reordenado)
			/*print_r($tagsAbiertosEnOrden);
			echo '<br>';*/

			// Guardamos en la noticia correspondinte su respectivo array de tags que no tuvieron cierre (reordenado)
			$noticias[$index]['tagsAbiertos'] = $tagsAbiertosEnOrden;

			// PRUEBA: Imprimimos el array que contiene la noticia para comprobar todo
			/*print_r($noticias[$index]);
			echo '<br>';
			echo '<br>';*/
		}

		$dataBody['paginaSolicitada'] = $pagina;
		$dataBody['cantidadDePaginas'] = $cantidadDePaginas;
		$dataBody['noticias'] = $noticias;

		// Cargamos los eventos que se muestran en la tabla
		$query = $this->db->query('select * from eventos where inicio >= curdate() and localizacion = "Nacional" order by inicio asc limit 3');
		$dataBody['eventosNacionales'] = $query->result_array();
		$query = $this->db->query('select * from eventos where inicio >= curdate() and localizacion = "Internacional" order by inicio asc limit 3');
		$dataBody['eventosInternacionales'] = $query->result_array();

		$this->load->view('web/header', $dataHeader);
		$this->load->view('web/noticias', $dataBody);
		$this->load->view('web/footer');
	}

	public function contacto()
	{
		$titulo = 'SEO';
		$dataHeader['titlePage'] = $titulo;

		$this->load->view('web/header', $dataHeader);
		$this->load->view('web/contacto');
		$this->load->view('web/footer');
	}

	public function enviarContactoWeb()
	{
        $nombre = $this->input->post("nombre");
        $email = $this->input->post("email");
        $asunto = $this->input->post("asunto");
        $mensaje = $this->input->post("mensaje");

        $this->enviarEmailContacto($nombre, $email, $asunto, $mensaje);
    }

    private function enviarEmailContacto($nombre = "Contacto", $email = "imera92@gmail.com", $asunto, $mensaje)
    {

        $this->load->library('email');

        // $config['protocol']  = 'smtp';
        // $config['smtp_host'] = 'ssl://mail.seo.com.ec '; // URL DEL HOST SMTP
        // $config['smtp_port'] = 26; // PUERTO SMTP DEL CLIENTE (va sin comillas)
        // $config['smtp_user'] = 'contacto@seo.com.ec'; // CORREO SMTP
        // $config['smtp_pass'] = 'TTuooa.123'; // CONTRASEÑA DEL EMAIL
        $config['charset']   = 'utf-8';
        $config['mailtype']  = 'html';
        $this->email->initialize($config);

        $this->email->set_newline("\r\n");
        $this->email->set_crlf("\r\n");

        $userDestinatario = $email;
        
        $cliente = $nombre;

        $postdata = http_build_query(
            array(
                'userNombre' => $nombre,
                'userAsunto' => $asunto,
                'userMensaje' => $mensaje,
                'userMail' => $email
            )
        );

        $opts = array('http' =>
            array(
                'method'  => 'POST',
                'header'  => 'Content-type: application/x-www-form-urlencoded',
                'content' => $postdata
            )
        );

        $context  = stream_context_create($opts);

        $cuerpoMail = file_get_contents(base_url("assets/mailings/mail-contacto.php"), false, $context);

        $this->email->from('contacto@seo.com.ec', 'Información SEO');
        $this->email->to($userDestinatario);
        $list_bcc = array('contacto@seo.com.ec');
        $this->email->bcc($list_bcc);
        $this->email->subject("Nuevo mail de contacto" . ' :: SEO');
        $this->email->message($cuerpoMail);
        $result = $this->email->send();

        if ($result) {
            $resultMail = array(
                'codigo' => 1,
                'mensaje' => "Correcto: El mensaje ha sido enviado",
            );
            redirect('/','refresh');
        }else{
            $resultMail = array(
                'codigo' => 0,
                'mensaje' => "Error: El mensaje no ha sido enviado",
            );
            redirect('/','refresh');
        }

        // header('Content-Type: application/json');
        // echo json_encode($resultMail);
    }

    public function revista()
	{
		$titulo = 'SEO';
		$dataHeader['titlePage'] = $titulo;

		$this->load->view('web/header', $dataHeader);
		$this->load->view('web/revista');
		$this->load->view('web/footer');
	}
}