	</div><!-- fin #wrapper -->
  
    <?php if (isset($js_files)): ?>
    
        <!-- grocerycrud -->
        <?php foreach($js_files as $file): ?>
            <script type="text/javascript" src="<?php echo $file; ?>"></script>
        <?php endforeach; ?>
        <!-- grocerycrud -->

        <?php if (isset($js_files_dd)): ?>
            <!-- Dependent Dropdown -->
            <?php echo $js_files_dd; ?>
            <!-- Dependent Dropdown -->            
        <?php endif ?>
    <?php else: ?>
        <!-- jQuery -->	       
        
    <?php endif ?>
    <script src="<?php echo base_url('public/js/bootstrap.min.js'); ?>"></script>
    <?php if ($this->router->method == 'socios' && $this->uri->segment(3) == 'read'): ?>
        <script src="<?php echo base_url('public/js/administrar_socios.js'); ?>"></script>
    <?php elseif ($this->router->method == 'carousel' && $this->uri->segment(3) == 'read'): ?>
        <script src="<?php echo base_url('public/js/administrar_carousel.js'); ?>"></script>
    <?php endif; ?>
    </body>
</html>