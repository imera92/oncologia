    <!-- Sidebar -->
    <div id="sidebar-wrapper">
        <ul class="sidebar-nav">
            <li class="sidebar-brand">
                <a href="<?php echo base_url('admin/index'); ?>">
                    <img class="img-responsive" src="<?php echo base_url('public/img/logo.png'); ?>">
                </a>
            </li>
            <li>
                <a href="<?php echo base_url('admin/carousel'); ?>"><i class="fa fa-picture-o" aria-hidden="true"></i>  Carousel</a>
            </li>
            <li>
                <a href="<?php echo base_url('admin/eventos'); ?>"><i class="fa fa-calendar" aria-hidden="true"></i> Eventos</a>
            </li>
            <li>
                <a href="<?php echo base_url('admin/archivos'); ?>"><i class="fa fa-file-o" aria-hidden="true"></i> Archivos</a>
            </li>
            <li>
                <a href="<?php echo base_url('admin/noticias'); ?>"><i class="fa fa-newspaper-o" aria-hidden="true"></i> Noticias</a>
            </li>
            <li>
                <a href="<?php echo base_url('admin/opiniones'); ?>"><i class="fa fa-comments-o" aria-hidden="true"></i> Opiniones</a>
            </li>
            <li>
                <a href="<?php echo base_url('admin/socios'); ?>"><i class="fa fa-users" aria-hidden="true"></i> Socios</a>
            </li>
            <li>
                <a href="#collapse1" data-toggle="collapse" ><i class="fa fa-external-link" aria-hidden="true"></i> Links de Interés</a>
            </li>
            <div id="collapse1" class="panel-collapse collapse">
                <ul class="sidebar-subnav list-group mb-0">
                    <li>
                        <a href="<?php echo base_url('admin/links_mbe'); ?>"> MBE</a>
                    </li>
                    <li>
                        <a href="<?php echo base_url('admin/links_oncologia'); ?>"> Oncología</a>
                    </li>
                </ul>
            </div>
            <li>
                <a href="<?php echo base_url('admin/logout'); ?>"><i class="fa fa-sign-out" aria-hidden="true"></i> Log out</a>
            </li>
        </ul>
    </div>
    <!-- /#sidebar-wrapper -->