	<!-- Page Content -->
	<div id="page-wrapper">
	    <div class="container-fluid">
	        <div class="row mt-10">
	            <div class="col-md-12">
	                <div class="box-inner">
	                    <?php if ( isset($output) ): ?>
	                        <div class="box-header well" data-original-title="">
	                            <h2>
	                            	<?php if($this->router->method == "socios"): ?>
	                            		<i class="fa fa-users" aria-hidden="true"></i>
                            		<?php elseif($this->router->method == "eventos"): ?>
                            			<i class="fa fa-calendar" aria-hidden="true"></i>
                        			<?php elseif($this->router->method == "opiniones"): ?>
                        				<i class="fa fa-comments-o" aria-hidden="true"></i>
                            		<?php endif; ?>
	                                &nbsp;&nbsp;<?php echo $titleCRUD; ?>
	                            </h2>
	                        </div>
	                        <div class="box-content">
	                            <?php echo $output; ?>
	                        </div>
	                    <?php else: ?>
	                        <div class="box-header well" data-original-title="">
	                            <h2>
	                                <i class="glyphicon glyphicon-warning-sign"></i>&nbsp;&nbsp;Atención
	                            </h2>
	                        </div>
	                        <div class="box-content text-center">
	                            <h3>No se encontró contenido</h3>
	                        </div>
	                    <?php endif ?>
	                </div>
	            </div>
	        </div><!--/row-->
	    </div>
	    <!-- /.container-fluid -->
	</div>
	<!-- /#page-wrapper -->