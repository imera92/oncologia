		<div class="row mt-lg-180 mt-md-140 mt-sm-130 mt-xs-180 mb-30">
			<div class="col-xs-10 col-xs-offset-1">
				<div class="row">
					<div class="col-xs-12">
						<h1 class="section-title">REVISTA ONCOLOGÍA</h1>
					</div>
				</div>
				<div class="row mt-30">
					<div class="col-xs-12">
						<div class="contentpane">
							<iframe 	id="blockrandom"
								name="iframe"
								src="http://seo.com.ec/old_site/revista/index.php/RevOncolEcu/index"
								width="100%"
								height="500"
								scrolling="auto"
								frameborder="1"
								class="wrapper">
								Esta opción no funcionará correctamente. Desafortunadamente su navegador no soporta IFRAMES.</iframe>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xs-12 mb-50">
				<div id="navpanel" class="row mt-60 text-center">
					<div class="col-xs-3 navpanel-container">
						<a class="navpanel-link" href="<?php echo base_url('eventos'); ?>">
							<div class="row mt-40 mb-20">
								<div class="col-xs-12">
									<span class="navpanel-title">EDUCACIÓN MÉDICA CONTINUA</span>
								</div>
							</div>
							<div class="row mb-40">
								<div class="col-xs-12">
									<span class="navpanel-text">Conoce los eventos que la Sociedad Ecuatoriana de Oncología tiene planificados</span>
								</div>
							</div>
						</a>
					</div>
					<div class="col-xs-3 navpanel-container">
						<a class="navpanel-link" href="<?php echo base_url('revista'); ?>">
							<div class="row mt-40 mb-20">
								<div class="col-xs-12">
									<span class="navpanel-title">REVISTA ONCOLOGÍA</span>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12">
									<span class="navpanel-text">Acceda al órgano oficial de comunicación de SOLCA</span>
								</div>
							</div>
						</a>
					</div>
					<div class="col-xs-3 navpanel-container">
						<a class="navpanel-link" href="http://www.salud.gob.ec/guias-de-practica-clinica/" target="_blank">
							<div class="row mt-40 mb-20">
								<div class="col-xs-12">
									<span class="navpanel-title">GUÍAS DE PRÁCTICA CLÍNICA</span>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12">
									<span class="navpanel-text">Ministerio de Salud Pública</span>
								</div>
							</div>
						</a>
					</div>
				</div>
			</div>
		</div>