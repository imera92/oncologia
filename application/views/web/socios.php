		<div class="row mt-lg-180 mt-md-140 mt-sm-130 mt-xs-180">
			<div class="col-xs-12 col-sm-7 col-sm-offset-1">
				<div class="row">
					<div class="col-xs-12">
						<h1 class="section-title">SOCIOS</h1>
					</div>
				</div>
				<div class="row mt-20">
					<div class="col-xs-12 col-sm-12 col-md-11">
						<div class="search-bar">
							<?php foreach (range('A', 'Z') as $letra): ?>
								<?php if (isset($pagina) && $letra == $pagina): ?>
									<span class="pagination-current"><?php echo $letra; ?></span>
								<?php else: ?>
									<a href="<?php echo base_url('socios/'.$letra); ?>"><?php echo $letra; ?></a>
								<?php endif; ?>
								<?php if ($letra != 'Z') echo ' - ' ?>
							<?php endforeach; ?>
						</div>
					</div>
				</div>
				<div class="row mt-20">
					<?php if (empty($sociosNacionales) && empty($sociosInternacionales)): ?>
						<div class='col-xs-12 no-data-warning'><span>NO HAY DATOS PARA MOSTRAR</span></div>
					<?php endif; ?>
					<?php foreach ($sociosNacionales as $index => $socio): ?>
						<div class="col-xs-6 col-md-4 box">
							<div class="profile-container mt-20">
								<?php 
								if ($socio['foto'] == ''){
									$fotoUrl = base_url("assets/uploads/socios/default.png");
								} else {
									$fotoUrl = base_url("assets/uploads/socios/".$socio['foto']);
								}
								?>
								<img class="img-responsive" src="<?php echo $fotoUrl; ?>">
								<div class="profile-footer mt-10">
									<?php $conteo = 0; ?>
									<br><span class="bold-text"><?php echo $socio['nombre'] . ' ' . $socio['apellido']; ?></span>
									<?php if (!empty($socio['telefono'])) { echo '<br>' . $socio['telefono']; } else { $conteo++; } ?>
									<?php if (!empty($socio['celular'])) { echo '<br>' . $socio['celular']; } else { $conteo++; } ?>
									<br><?php echo $socio['correo']; ?>
									<br><span class="last-label"><?php echo $socio['ciudad']; ?></span>
									<?php for ($i = 0; $i <= $conteo; $i++): ?>
										<br>
									<?php endfor; ?>
								</div>
							</div>
						</div>						
					<?php endforeach; ?>
					<?php foreach ($sociosInternacionales as $index => $socio): ?>
						<div class="col-xs-6 col-md-4 box">
							<div class="profile-container mt-20">
								<?php 
								if ($socio['foto'] == ''){
									$fotoUrl = base_url("assets/uploads/socios/default.png");
								} else {
									$fotoUrl = base_url("assets/uploads/socios/".$socio['foto']);
								}
								?>
								<img class="img-responsive" src="<?php echo $fotoUrl; ?>">
								<div class="profile-footer mt-10">
									<?php $conteo = 0; ?>
									<br><span class="bold-text"><?php echo $socio['nombre'] . ' ' . $socio['apellido']; ?></span>
									<?php if (!empty($socio['telefono'])) { echo '<br>' . $socio['telefono']; } else { $conteo++; } ?>
									<?php if (!empty($socio['celular'])) { echo '<br>' . $socio['celular']; } else { $conteo++; } ?>
									<br><?php echo $socio['correo']; ?>
									<br><span class="last-label"><?php echo $socio['ciudad']; ?></span>
									<?php for ($i = 0; $i <= $conteo; $i++): ?>
										<br>
									<?php endfor; ?>
								</div>
							</div>
						</div>						
					<?php endforeach; ?>
				</div>
			</div>
			<div class="col-xs-12 col-sm-3 pt-md-60 pt-lg-60">
				<div class="row navpanel">
					<div class=" col-xs-12 navpanel-container text-center">
						<a class="navpanel-link" href="<?php echo base_url('eventos'); ?>">
							<div class="row mt-xs-20 mb-xs-10 mt-40 mb-20">
								<div class="col-xs-12">
									<span class="navpanel-title">EDUCACIÓN MÉDICA CONTINUA</span>
								</div>
							</div>
							<div class="row mb-xs-20 mb-40">
								<div class="col-xs-12">
									<span class="navpanel-text">Conoce los eventos que la Sociedad Ecuatoriana de Oncología tiene planificados</span>
								</div>
							</div>
						</a>
					</div>
				</div>
				<div class="row navpanel">
					<div class=" col-xs-12 navpanel-container text-center">
						<a class="navpanel-link" href="<?php echo base_url('revista'); ?>">
							<div class="row mt-xs-20 mb-xs-10 mt-40 mb-20">
								<div class="col-xs-12">
									<span class="navpanel-title">REVISTA ONCOLOGÍA</span>
								</div>
							</div>
							<div class="row mb-xs-20 mb-40">
								<div class="col-xs-12">
									<span class="navpanel-text">Acceda al órgano oficial de comunicación de SOLCA</span>
								</div>
							</div>
						</a>
					</div>
				</div>
				<div class="row navpanel">
					<div class=" col-xs-12 navpanel-container text-center">
						<a class="navpanel-link" href="http://www.salud.gob.ec/guias-de-practica-clinica/" target="_blank">
							<div class="row mt-xs-20 mb-xs-10 mt-40 mb-20">
								<div class="col-xs-12">
									<span class="navpanel-title">GUÍAS DE PRÁCTICA CLÍNICA</span>
								</div>
							</div>
							<div class="row mb-xs-20 mb-40">
								<div class="col-xs-12">
									<span class="navpanel-text">Ministerio de Salud Pública</span>
								</div>
							</div>
						</a>
					</div>
				</div>
			</div>
		</div>
		<div class="row mt-60">
			<div class="col-sm-10 col-sm-offset-1">
				<h4 class="infotable-title">EVENTOS DE LA SOCIEDAD ECUATORIANA DE ONCOLOGÍA</h4>
			</div>
			<div class="col-sm-10 col-sm-offset-1 mt-20 table-responsive">
				<table class="table table-hover infotable">
				    <thead>
				      <tr>
				        <th>NOMBRE</th>
				        <th>TIPO</th>
				        <th>DESCRIPCIÓN</th>
				        <th class="fecha">INICIO</th>
				        <th class="fecha">FIN</th>
				      </tr>
				    </thead>
				    <tbody>
				    	<?php foreach ($eventosNacionales as $index => $evento): ?>
				    		<tr>
				    			<td class="bold-text"><?php echo $evento['nombre']; ?></td>
				    			<td><?php echo $evento['tipo']; ?></td>
				    			<td><?php echo $evento['descripcion']; ?></td>
				    			<td><?php echo $evento['inicio']; ?></td>
				    			<td><?php echo $evento['fin']; ?></td>
				    		</tr>
			    		<?php endforeach; ?>
				    </tbody>
			  	</table>
			</div>
		</div>
		<div class="row mt-30 mb-30">
			<div class="col-sm-10 col-sm-offset-1">
				<h4 class="infotable-title">EVENTOS INTERNACIONALES</h4>
			</div>
			<div class="col-sm-10 col-sm-offset-1 mt-20 table-responsive">
				<table class="table table-hover infotable">
				    <thead>
				      <tr>
				        <th>NOMBRE</th>
				        <th>TIPO</th>
				        <th>DESCRIPCIÓN</th>
				        <th class="fecha">INICIO</th>
				        <th class="fecha">FIN</th>
				      </tr>
				    </thead>
				    <tbody>
				    	<?php foreach ($eventosInternacionales as $index => $evento): ?>
				    		<tr>
				    			<td class="bold-text"><?php echo $evento['nombre']; ?></td>
				    			<td><?php echo $evento['tipo']; ?></td>
				    			<td><?php echo $evento['descripcion']; ?></td>
				    			<td><?php echo $evento['inicio']; ?></td>
				    			<td><?php echo $evento['fin']; ?></td>
				    		</tr>
			    		<?php endforeach; ?>
				    </tbody>
			  	</table>
			</div>
		</div>