		<div class="row mt-lg-180 mt-md-140 mt-sm-130 mt-xs-180 mb-30">
			<div class="col-xs-12 col-sm-7 col-sm-offset-1">
				<div class="row">
					<div class="col-xs-12">
						<h1 class="section-title">CONTÁCTANOS</h1>
					</div>
				</div>

				<div class="row mt-30">
					<div class="col-xs-10">
						<form class="form-horizontal" method="post" id="form_contacto_index" action="<?php echo site_url('web/enviarContactoWeb'); ?>">
							<div class="form-group">
								<label for="nombre" class="col-sm-4 col-md-3 control-label">Tu nombre:</label>
								<div class="col-sm-8 col-md-9">
									<input type="text" class="form-control" id="nombre" name="nombre">
								</div>
							</div>
							<div class="form-group">
								<label for="email" class="col-sm-4 col-md-3 control-label">Tu correo:</label>
								<div class="col-sm-8 col-md-9">
									<input type="email" class="form-control" id="email" name="email">
								</div>
							</div>
							<div class="form-group">
								<label for="asunto" class="col-sm-4 col-md-3 control-label">Un asunto:</label>
								<div class="col-sm-8 col-md-9">
									<input type="text" class="form-control" id="asunto" name="asunto">
								</div>
							</div>
							<div class="form-group">
								<label for="mensaje" class="col-sm-4 col-md-3 control-label">Tu mensaje:</label>
								<div class="col-sm-8 col-md-9">
									<textarea id="mensaje" class="form-control" rows="6" name="mensaje"></textarea>
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-offset-3 col-sm-9">
									<!-- LLAVE DE INTEGRACION DE PRUEBAS -->
									<!-- <div class="g-recaptcha" data-sitekey="6Lf-LyIUAAAAAFnKse2-VBKn5qgAIXW3O_lTS_8p" data-callback="recaptchaCallback"></div> -->
									
									<!-- LLAVE DE INTEGRACION DE PRODUCCION -->
									<div class="g-recaptcha" data-sitekey="6LcdTScUAAAAADCazzmUVUiryrxkeJJjFczV7HGZ" data-callback="recaptchaCallback"></div>
								</div>
							</div>
							<div class="form-group">
							    <div class="col-sm-offset-3 col-sm-9">
									<input id="submitContacto" type="submit" value="ENVIAR" disabled>
							    </div>
						    </div>
						</form>
					</div>
				</div>

			</div>
			<div class="col-xs-12 col-sm-3 pt-md-60 pt-lg-60">
				<div class="row navpanel">
					<div class=" col-xs-12 navpanel-container text-center">
						<a class="navpanel-link" href="<?php echo base_url('eventos'); ?>">
							<div class="row mt-xs-20 mb-xs-10 mt-40 mb-20">
								<div class="col-xs-12">
									<span class="navpanel-title">EDUCACIÓN MÉDICA CONTINUA</span>
								</div>
							</div>
							<div class="row mb-xs-20 mb-40">
								<div class="col-xs-12">
									<span class="navpanel-text">Conoce los eventos que la Sociedad Ecuatoriana de Oncología tiene planificados</span>
								</div>
							</div>
						</a>
					</div>
				</div>
				<div class="row navpanel">
					<div class=" col-xs-12 navpanel-container text-center">
						<a class="navpanel-link" href="<?php echo base_url('revista'); ?>">
							<div class="row mt-xs-20 mb-xs-10 mt-40 mb-20">
								<div class="col-xs-12">
									<span class="navpanel-title">REVISTA ONCOLOGÍA</span>
								</div>
							</div>
							<div class="row mb-xs-20 mb-40">
								<div class="col-xs-12">
									<span class="navpanel-text">Acceda al órgano oficial de comunicación de SOLCA</span>
								</div>
							</div>
						</a>
					</div>
				</div>
				<div class="row navpanel">
					<div class=" col-xs-12 navpanel-container text-center">
						<a class="navpanel-link" href="http://www.salud.gob.ec/guias-de-practica-clinica/" target="_blank">
							<div class="row mt-xs-20 mb-xs-10 mt-40 mb-20">
								<div class="col-xs-12">
									<span class="navpanel-title">GUÍAS DE PRÁCTICA CLÍNICA</span>
								</div>
							</div>
							<div class="row mb-xs-20 mb-40">
								<div class="col-xs-12">
									<span class="navpanel-text">Ministerio de Salud Pública</span>
								</div>
							</div>
						</a>
					</div>
				</div>
			</div>
		</div>