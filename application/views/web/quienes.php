		<div class="row mt-lg-180 mt-md-140 mt-sm-130 mt-xs-180">
			<div class="col-xs-12 col-sm-7 col-sm-offset-1">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-11 quienes-text">
						<h2 class="quienes-title">QUIÉNES SOMOS - NUESTRA MISIÓN</h2>
						<p>Agrupar en su seno a los profesionales de las Ciencias Médicas, que utilicen como o con fines diagnósticos, terapéuticos y de investigación de la Ciencia de la Oncología. Fomentar la práctica de la Oncología, en condiciones tradicionales de ética profesionales, dignidad y eficiencia, atendiéndose para ellos a lo prescrito en estos Estatutos y a las disposiciones establecidas por los Colegios de Médicos y de la Federación de Médicos del Ecuador. Desarrollar y tecnificar en metodologías, implementación de instrumentos metodológicos y/o equipamiento los estudios de la Oncología en el Ecuador. Al igual que educar, realizar cursos de educación médica continua, llámense symposium, congresos, cursos o actualizaciones continuas Contribuir al mejoramiento y bienestar de la población mediante la investigación y atención específica dirigida fundamentalmente a la población en riesgo del país.</p>
					</div>
				</div>
				<div class="row mt-20">
					<div class="col-xs-12 col-sm-12 col-md-11 quienes-text">
						<h2 class="quienes-title">NUESTRA VISIÓN</h2>
						<p>Ser un referente científico en Oncologia y llegar a ser un organismo consultor para la elaboración e implementación de normativa científicas y politicas publicas relacionadas a Oncología.</p>
					</div>
				</div>
				<div class="row mt-20">
					<div class="col-xs-12 col-sm-12 col-md-11 quienes-text">
						<h2 class="quienes-title">NUESTROS PRINCIPIOS</h2>
						<p>Excelencia, Investigación, Integridad, Compromiso, Servicio, Respeto, Interdisciplinariedad.</p>
					</div>
				</div>
				<div class="row mt-40">
					<div class="col-xs-12 col-md-11">
						<div class="owl-carousel owl-theme carousel">
							<div class="item"><img class="img-responsive" src="<?php echo base_url('public/img/quienes1.png'); ?>"></div>
							<div class="item"><img class="img-responsive" src="<?php echo base_url('public/img/quienes2.png'); ?>"></div>
							<div class="item"><img class="img-responsive" src="<?php echo base_url('public/img/quienes3.png'); ?>"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-3 pt-md-60 pt-lg-60">
				<div class="row navpanel">
					<div class=" col-xs-12 navpanel-container text-center">
						<a class="navpanel-link" href="<?php echo base_url('eventos'); ?>">
							<div class="row mt-xs-20 mb-xs-10 mt-40 mb-20">
								<div class="col-xs-12">
									<span class="navpanel-title">EDUCACIÓN MÉDICA CONTINUA</span>
								</div>
							</div>
							<div class="row mb-xs-20 mb-40">
								<div class="col-xs-12">
									<span class="navpanel-text">Conoce los eventos que la Sociedad Ecuatoriana de Oncología tiene planificados</span>
								</div>
							</div>
						</a>
					</div>
				</div>
				<div class="row navpanel">
					<div class=" col-xs-12 navpanel-container text-center">
						<a class="navpanel-link" href="<?php echo base_url('revista'); ?>">
							<div class="row mt-xs-20 mb-xs-10 mt-40 mb-20">
								<div class="col-xs-12">
									<span class="navpanel-title">REVISTA ONCOLOGÍA</span>
								</div>
							</div>
							<div class="row mb-xs-20 mb-40">
								<div class="col-xs-12">
									<span class="navpanel-text">Acceda al órgano oficial de comunicación de SOLCA</span>
								</div>
							</div>
						</a>
					</div>
				</div>
				<div class="row navpanel">
					<div class=" col-xs-12 navpanel-container text-center">
						<a class="navpanel-link" href="http://www.salud.gob.ec/guias-de-practica-clinica/" target="_blank">
							<div class="row mt-xs-20 mb-xs-10 mt-40 mb-20">
								<div class="col-xs-12">
									<span class="navpanel-title">GUÍAS DE PRÁCTICA CLÍNICA</span>
								</div>
							</div>
							<div class="row mb-xs-20 mb-40">
								<div class="col-xs-12">
									<span class="navpanel-text">Ministerio de Salud Pública</span>
								</div>
							</div>
						</a>
					</div>
				</div>
			</div>
		</div>
		<div class="row mt-md-60 mb-md-60 mt-lg-60 mb-lg-60">
			<div class="col-xs-12 container-carousel">
				<div class="owl-carousel owl-theme carousel">
					<?php foreach ($imagenesCarousel as $index => $imagen): ?>
						<div class="item">
						<?php if (isset($imagen['link'])): ?>
			  				<a href="<?php echo $imagen['link']; ?>" target="_blank"><img src="<?php echo base_url('assets/uploads/carousel/'.$imagen['imagen']); ?>" alt="<?php echo isset($imagen['alt']) ? $imagen['alt'] : ''; ?>"></a>
		  				<?php else: ?>
		  					<img src="<?php echo base_url('assets/uploads/carousel/'.$imagen['imagen']); ?>" alt="<?php echo isset($imagen['alt']) ? $imagen['alt'] : ''; ?>">
		  				<?php endif; ?>
		  				</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>