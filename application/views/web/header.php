<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta name="HandheldFriendly" content="true">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<title><?php echo $titlePage; ?></title>
	<link href="<?php echo base_url('public/css/bootstrap.min.css'); ?>" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url('public/css/custom-bootstrap-margin-padding.css'); ?>" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url('public/css/icons.css'); ?>" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url('public/css/header.css'); ?>" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url('public/css/footer.css'); ?>" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url('public/css/misc.css'); ?>" rel="stylesheet" type="text/css">
	<?php if($this->router->method == "index" || $this->router->method == "quienes"): ?>
		<link href="<?php echo base_url('public/owlcarousel/assets/owl.carousel.min.css'); ?>" rel="stylesheet" type="text/css">
		<link href="<?php echo base_url('public/owlcarousel/assets/owl.theme.default.css'); ?>" rel="stylesheet" type="text/css">
		<link href="<?php echo base_url('public/css/owl-carousel-custom-nav.css'); ?>" rel="stylesheet" type="text/css">
	<?php endif; ?>
	<?php if($this->router->method == "index"): ?>
		<link href="<?php echo base_url('public/css/home.css'); ?>" rel="stylesheet" type="text/css">
	<?php elseif ($this->router->method == "links"): ?>
		<link href="<?php echo base_url('public/css/links.css'); ?>" rel="stylesheet" type="text/css">
	<?php elseif ($this->router->method == "mbe"): ?>
		<link href="<?php echo base_url('public/css/mbe.css'); ?>" rel="stylesheet" type="text/css">
	<?php elseif ($this->router->method == "oncologia"): ?>
		<link href="<?php echo base_url('public/css/oncologia.css'); ?>" rel="stylesheet" type="text/css">
	<?php elseif ($this->router->method == "opinion"): ?>
		<link href="<?php echo base_url('public/css/opinion.css'); ?>" rel="stylesheet" type="text/css">
	<?php elseif ($this->router->method == "noticiasActuales"): ?>
		<link href="<?php echo base_url('public/css/noticias.css'); ?>" rel="stylesheet" type="text/css">
	<?php elseif ($this->router->method == "quienes"): ?>
		<link href="<?php echo base_url('public/css/quienes.css'); ?>" rel="stylesheet" type="text/css">
	<?php elseif ($this->router->method == "socios"): ?>
		<link href="<?php echo base_url('public/css/socios.css'); ?>" rel="stylesheet" type="text/css">
	<?php elseif ($this->router->method == "eventos"): ?>
		<link href="<?php echo base_url('public/css/eventos.css'); ?>" rel="stylesheet" type="text/css">
	<?php elseif ($this->router->method == "noticias"): ?>
		<link href="<?php echo base_url('public/css/post.css'); ?>" rel="stylesheet" type="text/css">
	<?php elseif ($this->router->method == "contacto"): ?>
		<link href="<?php echo base_url('public/css/contacto.css'); ?>" rel="stylesheet" type="text/css">
	<?php elseif ($this->router->method == "revista"): ?>
		<link href="<?php echo base_url('public/css/revista.css'); ?>" rel="stylesheet" type="text/css">
	<?php endif; ?>

	<?php if ($this->router->method == "contacto"): ?>
		<script src="https://www.google.com/recaptcha/api.js" async defer></script>
	<?php endif; ?>

	<script src="https://use.fontawesome.com/39a4a473d1.js"></script>
	<script type="text/javascript">
        var base_url = '<?php echo base_url(); ?>';

        var js_site_url = function( urlText ){
            var urlTmp = "<?php echo site_url('" + urlText + "'); ?>";
            return urlTmp;
        }

        var js_base_url = function( urlText ){
            var urlTmp = "<?php echo base_url('" + urlText + "'); ?>";
            return urlTmp;
        }
    </script>
</head>
<body>
	<div class="container-fluid">
		<div id="header">
			<div class="row mt-10 mb-10 centered-text">
				<div class="col-xs-6 col-xs-offset-3 col-sm-4 col-sm-offset-1 col-md-4 col-md-offset-1">
					<a href="<?php echo base_url(''); ?>"><img id="header-logo" class="img-responsive" src="<?php echo base_url('public/img/logo.png'); ?>"></a>
				</div>
				<div class="col-xs-12 col-sm-7">
					<div class="row pl-xxs-20">
						<div class="col-xs-4 col-sm-3 col-sm-offset-3 pl-0 pr-0 data menubar-text"><span class="glyphicon glyphicon glyphicon-earphone" aria-hidden="true"></span> +593 99 295 0013</div>
						<div class="col-xs-4 col-sm-3 pl-0 pr-0 data menubar-text"><a class="mailto-link" href="mailto:info@seo.com.ec?Subject=Más%20información"><span class="glyphicon glyphicon glyphicon glyphicon-envelope" aria-hidden="true"></span> info@seo.com.ec</a></div>
						<div class="col-xs-4 col-sm-3 pl-0 data menubar-text centered-text">
							<a class="menubar-link-social" href="https://twitter.com/Oncologia_Ec" target="_blank"><span class="icons-tw"></span></a>
							<a class="menubar-link-social" href="https://www.facebook.com/oncologiaec/" target="_blank"><span class="icons-fb"></span></a>
						</div>						
					</div>
				</div>
			</div>
			<div id="navbar" class="row">
				<div class="col-xs-12 pl-0 pr-0">
					<nav class="navbar navbar-default mb-0">
						<div class="container-fluid">
							<div class="navbar-header">
						      	<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menubar-collapse" aria-expanded="false">
						        	<span class="sr-only">Toggle navigation</span>
						        	<span class="icon-bar"></span>
						        	<span class="icon-bar"></span>
						        	<span class="icon-bar"></span>
						    	</button>
						    </div>
							<div class="collapse navbar-collapse" id="menubar-collapse">
								<ul class="nav navbar-nav">
									<li><a href="<?php echo base_url(''); ?>">INICIO</a></li>
									<li><a href="<?php echo base_url('quienes'); ?>">QUIÉNES SOMOS</a></li>
									<li class="dropdown">
									 	<a href="http://www.primeoncology.org/" class="dropdown-toggle" id="dropdownMenu1" data-toggle="dropdown">
									 		INTERÉS CIENTÍFICO<span class="caret"></span>
								    	</a>
										<ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu1">
									    	<li><a href="<?php echo base_url('opinion/1'); ?>">NUESTROS SOCIOS OPINAN</a></li>
									    	<li><a href="<?php echo base_url('noticiasActuales/1'); ?>">NOTICIAS ACTUALES</a></li>
									    	<li class="dropdown-submenu">
									    		<a tabindex="-1" href="#">
									    			LINKS DE INTERÉS
								    			</a>
								    			<ul class="dropdown-menu">
								    				<li><a href="<?php echo base_url('links'); ?>">LINKS</a></li>
								    				<li class="dropdown-submenu">
								    					<a tabindex="-1" href="#">INFORMACION ONLINE</a>
								    					<ul class="dropdown-menu">
								    						<li><a href="<?php echo base_url('mbe'); ?>">MEDICINA BASADA EN EVIDENCIA (MBE)</a></li>
								    						<li><a href="<?php echo base_url('oncologia'); ?>">ONCOLOGÍA</a></li>
								    					</ul>
							    					</li>
								    			</ul>
								    		</li>
									    </ul>
									</li>
									<li><a href="<?php echo base_url('socios'); ?>">SOCIOS</a></li>
									<li><a href="<?php echo base_url('eventos'); ?>">EVENTOS</a></li>
									<li><a href="<?php echo base_url('contacto'); ?>">CONTACTO</a></li>
									<li><a href="<?php echo base_url('revista'); ?>">REVISTA ONCOLOGÍA</a></li>
								</ul>
							</div>
						</div>
					</nav>
				</div>
			</div>
		</div>