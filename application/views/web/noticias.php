		<div class="row mt-lg-180 mt-md-140 mt-sm-130 mt-xs-180">
			<div class="col-xs-12 col-sm-7 col-sm-offset-1">
				<div class="row">
					<div class="col-xs-12">
						<h1 class="section-title">INTERÉS CIENTÍFICO - NOTICIAS MÉDICAS ACTUALES</h1>
					</div>
				</div>
				<div class="row mt-20">
					<?php $gray = 1; ?>
					<?php foreach ($noticias as $index => $noticia): ?>
						<div class="col-xs-12 col-sm-12 col-md-11 post-text">
							<div class="well <?php echo $gray == 1 ? 'post-background-gray' : 'post-background-white' ?>">
				    			<h2 class="post-title"><?php echo $noticia['titulo']; ?></h2>
				    			<?php //if (!empty($noticia['imagen'])): ?>
				    				<!-- <img class="img-responsive mb-20" src="<?php //echo base_url('assets/uploads/noticias/' . $noticia['imagen']); ?>"> -->
			    				<?php //endif; ?>
				    			<div>
				    				<?php echo $noticia['texto']; ?>
				    				<?php foreach ($noticia['tagsAbiertos'] as $index => $tag): ?>
				    					<?php echo '</' . $tag . '>'; ?>				    				
			    					<?php endforeach; ?>
				    			</div>
								<a href="<?php echo base_url('noticias/' . $noticia['id']); ?>" class="readmore-link"><span>Leer más &gt&gt</span></a>
							</div>
						</div>
						<?php if ($gray == 1) {
							$gray = 0;
						} else {
							$gray = 1;	
						} ?>
		    		<?php endforeach; ?>
				</div>
				<div class="row">
					<div class="col-xs-11 pagination-container">
						<?php for ($i=1; $i <= $cantidadDePaginas; $i++): ?>
							<?php if ($i == $paginaSolicitada): ?>
								<span class="pagination-current">[<?php echo $i ?>]</span>
							<?php else: ?>
								<a class="pagination-link" href="<?php echo base_url('noticiasActuales/'.$i); ?>">[<?php echo $i ?>]</a>
							<?php endif; ?>
						<?php endfor; ?>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-3 pt-md-60 pt-lg-60">
				<div class="row navpanel">
					<div class=" col-xs-12 navpanel-container text-center">
						<a class="navpanel-link" href="<?php echo base_url('eventos'); ?>">
							<div class="row mt-xs-20 mb-xs-10 mt-40 mb-20">
								<div class="col-xs-12">
									<span class="navpanel-title">EDUCACIÓN MÉDICA CONTINUA</span>
								</div>
							</div>
							<div class="row mb-xs-20 mb-40">
								<div class="col-xs-12">
									<span class="navpanel-text">Conoce los eventos que la Sociedad Ecuatoriana de Oncología tiene planificados</span>
								</div>
							</div>
						</a>
					</div>
				</div>
				<div class="row navpanel">
					<div class=" col-xs-12 navpanel-container text-center">
						<a class="navpanel-link" href="<?php echo base_url('revista'); ?>">
							<div class="row mt-xs-20 mb-xs-10 mt-40 mb-20">
								<div class="col-xs-12">
									<span class="navpanel-title">REVISTA ONCOLOGÍA</span>
								</div>
							</div>
							<div class="row mb-xs-20 mb-40">
								<div class="col-xs-12">
									<span class="navpanel-text">Acceda al órgano oficial de comunicación de SOLCA</span>
								</div>
							</div>
						</a>
					</div>
				</div>
				<div class="row navpanel">
					<div class=" col-xs-12 navpanel-container text-center">
						<a class="navpanel-link" href="http://www.salud.gob.ec/guias-de-practica-clinica/" target="_blank">
							<div class="row mt-xs-20 mb-xs-10 mt-40 mb-20">
								<div class="col-xs-12">
									<span class="navpanel-title">GUÍAS DE PRÁCTICA CLÍNICA</span>
								</div>
							</div>
							<div class="row mb-xs-20 mb-40">
								<div class="col-xs-12">
									<span class="navpanel-text">Ministerio de Salud Pública</span>
								</div>
							</div>
						</a>
					</div>
				</div>
			</div>
		</div>
		<div class="row mt-60">
			<div class="col-sm-10 col-sm-offset-1">
				<h4 class="infotable-title">EVENTOS DE LA SOCIEDAD ECUATORIANA DE ONCOLOGÍA</h4>
			</div>
			<div class="col-sm-10 col-sm-offset-1 mt-20 table-responsive">
				<table class="table table-hover infotable">
				    <thead>
				      <tr>
				        <th>NOMBRE</th>
				        <th>TIPO</th>
				        <th>DESCRIPCIÓN</th>
				        <th class="fecha">INICIO</th>
				        <th class="fecha">FIN</th>
				      </tr>
				    </thead>
				    <tbody>
				    	<?php foreach ($eventosNacionales as $index => $evento): ?>
				    		<tr>
				    			<td class="bold-text"><?php echo $evento['nombre']; ?></td>
				    			<td><?php echo $evento['tipo']; ?></td>
				    			<td><?php echo $evento['descripcion']; ?></td>
				    			<td><?php echo $evento['inicio']; ?></td>
				    			<td><?php echo $evento['fin']; ?></td>
				    		</tr>
			    		<?php endforeach; ?>
				    </tbody>
			  	</table>
			</div>
			</div>
		</div>
		<div class="row mt-30 mb-30">
			<div class="col-sm-10 col-sm-offset-1">
				<h4 class="infotable-title">EVENTOS INTERNACIONALES</h4>
			</div>
			<div class="col-sm-10 col-sm-offset-1 mt-20 table-responsive">
				<table class="table table-hover infotable">
				    <thead>
				      <tr>
				        <th>NOMBRE</th>
				        <th>TIPO</th>
				        <th>DESCRIPCIÓN</th>
				        <th class="fecha">INICIO</th>
				        <th class="fecha">FIN</th>
				      </tr>
				    </thead>
				    <tbody>
				    	<?php foreach ($eventosInternacionales as $index => $evento): ?>
				    		<tr>
				    			<td class="bold-text"><?php echo $evento['nombre']; ?></td>
				    			<td><?php echo $evento['tipo']; ?></td>
				    			<td><?php echo $evento['descripcion']; ?></td>
				    			<td><?php echo $evento['inicio']; ?></td>
				    			<td><?php echo $evento['fin']; ?></td>
				    		</tr>
			    		<?php endforeach; ?>
				    </tbody>
			  	</table>
			</div>
		</div>