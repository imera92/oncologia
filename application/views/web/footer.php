		<div id="footer" class="row pt-40 pb-40">
			<div class="col-md-12">
				<div class="row">
					<div class="col-xs-6 col-xs-offset-0 col-sm-2 col-sm-offset-1 col-md-2 col-md-offset-1 col-lg-2 col-lg-offset-1">
						<img class="img-responsive" src="<?php echo base_url('public/img/logo-blanco.png'); ?>">
					</div>
					<div class="col-xs-6 col-sm-3 col-md-3 col-lg-3 mt-xs-20">
						<span class="bold-text">MÁS SOBRE NOSOTROS</span>
						<br>
						<ul class="pl-20">
							<li><a href="<?php echo base_url('quienes'); ?>">Quiénes somos</a></li>
							<li><a href="<?php echo base_url('eventos'); ?>">Educación Médica Continua</a></li>
							<li><a href="<?php echo base_url('revista'); ?>">Revista de Oncología</a></li>
							<li><a href="<?php echo base_url('socios'); ?>">Doctores</a></li>
						</ul>
					</div>
					<div class="col-xs-6 col-xs-offset-0 col-sm-3 col-sm-offset-0 col-md-3 col-md-offset-0 col-lg-3 col-lg-offset-0 mt-xs-20">
						<span class="bold-text">LINKS DE INTERÉS</span>
						<br>
						<ul class="pl-20">
							<li><a href="#">BVS Ecuador</a></li>
							<li><a href="#">Pubmed</a></li>
							<li><a href="#">American Society of Clinical Oncology</a></li>
							<li><a href="#">American Association for Cancer Research</a></li>
							<li><a href="#">Sociedad Latinoamericana de Oncología Médica</a></li>
						</ul>
					</div>
					<div class="col-xs-6 col-sm-3 col-md-3 col-lg-3 mt-xs-20">
						<span class="bold-text">CONTÁCTENOS</span>
						<br>
						<ul class="pl-20">
							<li>+593 04 220 4395</li>
							<li>info@seo.com.ec</li>
							<li>Núcleo Guayas</li>
							<li>Dra. Katherine García</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
    <script src="<?php echo base_url('public/js/jquery-3.2.1.min.js'); ?>"></script>
    <script src="<?php echo base_url('public/js/bootstrap.min.js'); ?>"></script>
    <script src="<?php echo base_url('public/js/navbar.js'); ?>"></script>
    <?php if($this->router->method == "index"): ?>
    	<script src="<?php echo base_url('public/js/home.js'); ?>"></script>
	<?php elseif ($this->router->method == "socios"): ?>
    	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery.matchHeight/0.7.0/jquery.matchHeight-min.js"></script>
		<script src="<?php echo base_url('public/js/socios.js'); ?>"></script>
	<?php elseif ($this->router->method == "contacto"): ?>
		<script src="<?php echo base_url('public/js/jquery.validate.min.js'); ?>"></script>
		<script src="<?php echo base_url('public/js/contacto.js'); ?>"></script>
	<?php endif; ?>
    <?php if($this->router->method == "index" || $this->router->method == "interes" || $this->router->method == "quienes"): ?>
	    <script src="<?php echo base_url('public/owlcarousel/owl.carousel.min.js'); ?>"></script>
	    <script src="<?php echo base_url('public/js/script.js'); ?>"></script>
	<?php endif; ?>
</body>
</html>