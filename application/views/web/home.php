		<div class="row mt-lg-150 mt-md-140 mt-sm-130 mt-xs-180">
			<div class="col-xs-12 container-carousel">
				<div class="owl-carousel owl-theme carousel">
					<?php foreach ($imagenesCarousel as $index => $imagen): ?>
						<div class="item">
						<?php if (isset($imagen['link'])): ?>
			  				<a href="<?php echo $imagen['link']; ?>" target="_blank"><img src="<?php echo base_url('assets/uploads/carousel/'.$imagen['imagen']); ?>" alt="<?php echo isset($imagen['alt']) ? $imagen['alt'] : ''; ?>"></a>
		  				<?php else: ?>
		  					<img src="<?php echo base_url('assets/uploads/carousel/'.$imagen['imagen']); ?>" alt="<?php echo isset($imagen['alt']) ? $imagen['alt'] : ''; ?>">
		  				<?php endif; ?>
		  				</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
		<div id="navpanel" class="row mt-60 text-center">
			<div class="col-xs-3 navpanel-container">
				<a class="navpanel-link" href="<?php echo base_url('eventos'); ?>">
					<div class="row mt-40 mb-20">
						<div class="col-xs-12">
							<span class="navpanel-title">EDUCACIÓN MÉDICA CONTINUA</span>
						</div>
					</div>
					<div class="row mb-40">
						<div class="col-xs-12">
							<span class="navpanel-text">Conoce los eventos que la Sociedad Ecuatoriana de Oncología tiene planificados</span>
						</div>
					</div>
				</a>
			</div>
			<div class="col-xs-3 navpanel-container">
				<a class="navpanel-link" href="<?php echo base_url('revista'); ?>">
					<div class="row mt-40 mb-20">
						<div class="col-xs-12">
							<span class="navpanel-title">REVISTA ONCOLOGÍA</span>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<span class="navpanel-text">Acceda al órgano oficial de comunicación de SOLCA</span>
						</div>
					</div>
				</a>
			</div>
			<div class="col-xs-3 navpanel-container">
				<a class="navpanel-link" href="http://www.salud.gob.ec/guias-de-practica-clinica/" target="_blank">
					<div class="row mt-40 mb-20">
						<div class="col-xs-12">
							<span class="navpanel-title">GUÍAS DE PRÁCTICA CLÍNICA</span>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<span class="navpanel-text">Ministerio de Salud Pública</span>
						</div>
					</div>
				</a>
			</div>
		</div>
		<div class="row mt-60 centered-text">
			<div class="col-sm-4 col-sm-offset-1 col-xs-6 col-xs-offset-3">
				<img class="img-responsive" src="<?php echo base_url('public/img/img2.jpg'); ?>">
			</div>
			<div id="directiva" class="col-sm-4 col-sm-offset-1 col-xs-8 col-xs-offset-2 mt-40 mt-md-10 mt-sm-40">
				<h4 class="infopanel-title">DIRECTIVA DE LA SOCIEDAD ECUATORIANA DE ONCOLOGÍA</h4>
				<div class="row mt-40 mt-md-20 mt-sm-10 infopanel-text">
					<span class="bold-text">Presidente</span> <span id="presidente"></span>
				</div>
				<div class="row mt-20 mt-md-20 mt-sm-5 infopanel-text">
					<span class="bold-text">Vicepresidente</span> <span id="vicepresidente"></span>
				</div>
				<div class="row mt-20 mt-md-20 mt-sm-5 infopanel-text">
					<span class="bold-text">Secretario</span> <span id="secretario"></span>
				</div>
				<div class="row mt-20 mt-md-20 mt-sm-5 infopanel-text">
					<span class="bold-text">Tesorero</span> <span id="tesorero"></span>
				</div>
				<div class="row mt-20 mt-md-20 mt-sm-5 infopanel-text">
					<span class="bold-text">Vocales Principales</span> <span id="principales"></span>
				</div>
				<div class="row mt-20 mt-md-20 mt-sm-5 infopanel-text">
					<span id="suplentes-label" class="bold-text">Vocales Suplentes</span> <span id="suplentes"></span>
				</div>
				<div class="row mt-40 mt-md-20 mt-sm-10">
					<button class="infopanel-link click-this">GUAYAS</button> - <button class="infopanel-link">AZUAY (NACIONAL)</button> - <button class="infopanel-link">MANABí</button> - <button class="infopanel-link">PICHINCHA</button>
				</div>
			</div>
		</div>
		<div class="row mt-60">
			<div class="col-sm-10 col-sm-offset-1">
				<h4 class="infotable-title">EVENTOS DE LA SOCIEDAD ECUATORIANA DE ONCOLOGÍA</h4>
			</div>
			<div class="col-sm-10 col-sm-offset-1 mt-20 table-responsive">
				<table class="table table-hover infotable">
				    <thead>
				      <tr>
				        <th>NOMBRE</th>
				        <th>TIPO</th>
				        <th>DESCRIPCIÓN</th>
				        <th class="fecha">INICIO</th>
				        <th class="fecha">FIN</th>
				      </tr>
				    </thead>
				    <tbody>
				    	<?php foreach ($eventosNacionales as $index => $evento): ?>
				    		<tr>
				    			<td class="bold-text"><?php echo $evento['nombre']; ?></td>
				    			<td><?php echo $evento['tipo']; ?></td>
				    			<td><?php echo $evento['descripcion']; ?></td>
				    			<td><?php echo $evento['inicio']; ?></td>
				    			<td><?php echo $evento['fin']; ?></td>
				    		</tr>
			    		<?php endforeach; ?>
				    </tbody>
			  	</table>
			</div>
		</div>
		<div class="row mt-60">
			<div class="col-sm-10 col-sm-offset-1">
				<h4 class="infotable-title">EVENTOS INTERNACIONALES</h4>
			</div>
			<div class="col-sm-10 col-sm-offset-1 mt-20 table-responsive">
				<table class="table table-hover infotable">
				    <thead>
				      <tr>
				        <th>NOMBRE</th>
				        <th>TIPO</th>
				        <th>DESCRIPCIÓN</th>
				        <th class="fecha">INICIO</th>
				        <th class="fecha">FIN</th>
				      </tr>
				    </thead>
				    <tbody>
				    	<?php foreach ($eventosInternacionales as $index => $evento): ?>
				    		<tr>
				    			<td class="bold-text"><?php echo $evento['nombre']; ?></td>
				    			<td><?php echo $evento['tipo']; ?></td>
				    			<td><?php echo $evento['descripcion']; ?></td>
				    			<td><?php echo $evento['inicio']; ?></td>
				    			<td><?php echo $evento['fin']; ?></td>
				    		</tr>
			    		<?php endforeach; ?>
				    </tbody>
			  	</table>
			</div>
		</div>