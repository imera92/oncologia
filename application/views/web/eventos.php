		<div class="row mt-lg-180 mt-md-140 mt-sm-130 mt-xs-180">
			<div class="col-xs-12 col-sm-7 col-sm-offset-1">
				<div class="row">
					<div class="col-xs-12">
						<h1 class="section-title">EVENTOS NACIONALES E INTERACIONALES</h1>
					</div>
				</div>
			</div>
		</div>
		<div class="row mt-10">
			<div class="col-xs-12 col-xs-offset-0 col-md-10 col-md-offset-1 centered-text">
				<a class="filter" href="<?php echo base_url('web/eventos/azuay'); ?>">Azuay</a>
				<a class="filter" href="<?php echo base_url('web/eventos/guayas'); ?>">Guayas</a>
				<a class="filter" href="<?php echo base_url('web/eventos/manabi'); ?>">Manabí</a>
				<a class="filter" href="<?php echo base_url('web/eventos/pichincha'); ?>">Pichincha</a>
			</div>
		</div>
		<div class="row mt-10">
			<div class="col-sm-12 col-sm-offset-0 col-md-10 col-md-offset-1 mt-20 table-responsive">
				<table class="table table-hover infotable">
				    <thead>
				      <tr>
				        <th>#</th>
				        <th>NOMBRE</th>
				        <th>TIPO</th>
				        <th>DESCRIPCIÓN</th>
				        <th>LOCALIZACIÓN</th>
				        <th class="fecha">INICIO</th>
				        <th class="fecha">FIN</th>
				        <th>CONTACTO</th>
				      </tr>
				    </thead>
				    <tbody>
				    	<?php foreach ($eventos as $index => $evento): ?>
				    		<tr>
				    			<td><?php echo $evento['id']; ?></td>
				    			<td><?php echo $evento['nombre']; ?></td>
				    			<td><?php echo $evento['tipo']; ?></td>
				    			<td><?php echo $evento['descripcion']; ?></td>
				    			<td><?php echo ($evento['localizacion'] == 'manabi') ? 'Manabí' : ucfirst($evento['localizacion']); ?></td>
				    			<td><?php echo $evento['inicio']; ?></td>
				    			<td><?php echo $evento['fin']; ?></td>
				    			<td><?php echo $evento['contacto']; ?></td>
				    		</tr>
			    		<?php endforeach; ?>
				    </tbody>
			  	</table>
			</div>
		</div>