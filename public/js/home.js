$(document).ready(function(){
	$('button[class~="click-this"]').click();
});

$('button[class~="infopanel-link"]').on('click', function(){
	$('button[class~="infopanel-link"]').removeClass('infopanel-link-active');
	$(this).addClass('infopanel-link-active');
	switch ($(this).html()) {
		case 'GUAYAS (NACIONAL)':
			$('#presidente').html('Dr. Jorge Moncayo');
			$('#vicepresidente').html('Dra. Mayra Santacruz');
			$('#secretario').html('Dr. Francisco Plaza');
			$('#tesorero').html('Dr. Salvador Encalada');
			$('#principales').html('Dr. Antonio Jurado, Dra. Katherine Garcia, Dra. Patricia Rizzo');
			$('#suplentes').html('Dr. Juan Carlos Ruiz, Dr. Holwin Solórzano, Dr. Dennis Layana');
			$('#suplentes-label').show();
			break;
		case 'PICHINCHA':
			$('#presidente').html('Dra. Tania Soria');
			$('#vicepresidente').html('Dr. Carlos Mejía');
			$('#secretario').html('Dra. Paulina Irigoyen');
			$('#tesorero').html('Dra. Miriam Argüello');
			$('#principales').html('Dr. Alex Vasconez, Dr. Fernando Checa, Dr. Carlos Eugenio');
			$('#suplentes').html('Dr. Luis Pacheco, Dr. Diego Pinto, Dr. Christian Pais, Dr. Ricardo Tixi');
			$('#suplentes-label').show();
			break;
		case 'MANABí':
			$('#presidente').html('Dr. Daniel Alarcón');
			$('#vicepresidente').html('Dr. Patricio Miranda');
			$('#secretario').html('Dra. Lorena Mejía');
			$('#tesorero').html('Dr. Miguel Cedeño');
			$('#principales').html('Dra. Jennifer Zambrano, Dra. Mariuxi Mendoza, Dr. Alex Torres, Dr. Jhon Torres');
			$('#suplentes').html('');
			$('#suplentes-label').hide();
			break;
		case 'AZUAY':
			$('#presidente').html('Dr. Humberto Quito');
			$('#vicepresidente').html('Dr. Juan Pablo Zapata');
			$('#secretario').html('Dra. Maria Isabel Leon');
			$('#tesorero').html('Dra. Rocío Murillo');
			$('#principales').html('Dr. Andres Rodriguez, Dra. Maria Eugenia Jaramillo, Dr. Felipe Murillo, Dr. Andres Andrade, Dr. Miguel Jerves, Dra. Johanna Saquisilí');
			$('#suplentes').html('');
			$('#suplentes-label').hide();
			break;
	}
});