$(window).bind('scroll', function() {
    if ($(window).scrollTop() > 150) {
        $('#header-logo').addClass('header-logo-scrolled');
        $('#header div[class~="data"]').removeClass('menubar-text');
        $('#header div[class~="data"]').addClass('menubar-text-scrolled');
        if($( document ).width() >= 750){
            $('#navbar nav[class~="navbar"]').addClass('navbar-scrolled');
            $('ul[class~="nav"]>li>a').addClass('nav-link-scrolled');            
        }
    }
    else {
        $('#header-logo').removeClass('header-logo-scrolled');
        $('#header div[class~="data"]').addClass('menubar-text');
        $('#header div[class~="data"]').removeClass('menubar-text-scrolled');
        $('#navbar nav[class~="navbar"]').removeClass('navbar-scrolled');
        $('ul[class~="nav"]>li>a').removeClass('nav-link-scrolled');
    }
});