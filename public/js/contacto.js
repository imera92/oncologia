$("#form_contacto_index").validate({
			rules: {
				nombre: "required",
				email: {
					required: true,
					email: true
				},
				asunto: "required",
				mensaje: {
					required: true,
					minlength: 2
				}
			},
			messages: {
				nombre: "Ingrese su nombre",
				email: {
					required: "Ingrese un correo para contactarlo",
					email: "Ingrese un correo valido"
				},
				asunto: "Ingrese un asunto para el mensaje",
				mensaje: {
					required: "Diganos en un mensaje en que esta interesado",
					minlength: "El mensaje debe tener un minimo de caracteres"
				},
			}
		});

var recaptchaCallback = function() {
	$('#submitContacto').removeAttr('disabled');
};