$(document).ready(function(){
	$('.carousel').owlCarousel({
		autoplay:true,
		autoplayTimeout:3000,
		center: true,
		dots:true,
		loop:true,	
		navText: ["<img class='img-responsive' src="+js_base_url('public/img/prev.png')+">","<img class='img-responsive' src="+js_base_url('public/img/next.png')+">"],
		margin: 10,
		responsiveClass:true,
		responsive:{
	        0:{
	            items:1,
	            nav:true
	        },
	        600:{
	            items:1,
	            nav:true
	        },
	        1000:{
	            items:1,
	            nav:true
	        }
	    }
	});
});